package parser

import scala.collection.mutable
import scala.collection.mutable.ArrayBuilder
import scala.collection.mutable.ArrayBuffer

/*
 Helper class to construct new instances by incrementally storing the needed information
 */
class InstanceData (val lastWord: Int) {
  val wordarr: mutable.ArrayBuilder.ofRef[String] = new ArrayBuilder.ofRef()
  //    val lemmasarr: mutable.ArrayBuilder.ofRef[String] = new ArrayBuilder.ofRef()
  val cposarr: mutable.ArrayBuilder.ofRef[String] = new ArrayBuilder.ofRef()
  val posarr: mutable.ArrayBuilder.ofRef[String] = new ArrayBuilder.ofRef()
  val feats: ArrayBuffer[Array[String]] = new ArrayBuffer()
  // heads needs to be accessed and changed, so no builder
  val heads: ArrayBuffer[Int] = ArrayBuffer[Int]()
  val deprel: mutable.ArrayBuilder.ofRef[String] = new ArrayBuilder.ofRef()
  def doAppend(inst: DependencyInstance,  offset: Int, head: Int) = {
    wordarr += inst.forms(offset)
      //      lemmasarr += inst.lemmas(offset)
    cposarr += inst.cpostags(offset)
    posarr += inst.postags(offset)
    feats += inst.feats(offset)
    // heads is special because we have to map it sometimes
    heads += head
    deprel += inst.deprels(offset)
  }
  /**
    *  produces a non-finalized instance setInstIds has to be called seperately (because we don't have a parser). 
    */
  def toInstance(): DependencyInstance = {
    val myFeats = Array[Array[String]]()
    // feats.toArray(Array[Array[String]]())
    new DependencyInstance(wordarr.result, /*lemmasarr.result*/ null, cposarr.result, posarr.result, feats.toArray, heads.toArray, deprel.result, lastWord)
  }
}

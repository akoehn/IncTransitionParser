package parser

import com.github.benmanes.caffeine.cache.CacheLoader
import com.github.benmanes.caffeine.cache.Caffeine
import com.github.benmanes.caffeine.cache.LoadingCache
import parser.feature.SyntacticFeatureFactory
import utils.FeatureVector

sealed trait LocalFeatureCacheKey {
}
// LocalFeatureKeys
case class ArcKey(h: Int, m: Int) extends LocalFeatureCacheKey
case class TripsKey(h: Int, m: Int, s: Int) extends LocalFeatureCacheKey
case class SibKey(m: Int, s: Int) extends LocalFeatureCacheKey
case class GPCKey(gp: Int, h: Int, m: Int) extends LocalFeatureCacheKey
case class HeadBiKey(h: Int, m: Int, h2: Int) extends LocalFeatureCacheKey
case class GPSibKey(gp: Int, h: Int, m: Int, s: Int) extends LocalFeatureCacheKey
case class TriSibKey(h: Int, m: Int, s1: Int, s2: Int) extends LocalFeatureCacheKey
case class GGPCKey(ggp: Int, gp: Int, h: Int, m: Int) extends LocalFeatureCacheKey
case class PSCKey(h: Int, m: Int, c: Int, sib: Int) extends LocalFeatureCacheKey
case class LabelKey(head: Int, mod: Int, typ: Int) extends LocalFeatureCacheKey
// case class CnKey(h: Int, m: Int) extends LocalFeatureCacheKey
// GlobalFeature keys
case class NeighborKey(par: Int, h: Int, left: Int, right: Int) extends LocalFeatureCacheKey
case class PPKey(gp: Int, h: Int, m: Int) extends LocalFeatureCacheKey
case class CC1Key(left: Int, arg: Int, right: Int) extends LocalFeatureCacheKey
case class CC2Key(arg: Int, head: Int, child: Int) extends LocalFeatureCacheKey
case class PNXKey(head: Int, arg: Int, pair: Int) extends LocalFeatureCacheKey
case class SpanKey(h: Int, end: Int, punc: Int, bin: Int) extends LocalFeatureCacheKey
case class CNKey(h: Int, leftNum: Int, rightNum: Int) extends LocalFeatureCacheKey


class IncLocalFeatureCacheLoader(var inst: DependencyInstance, var synFactory: SyntacticFeatureFactory)
  extends CacheLoader[LocalFeatureCacheKey, FeatureVector] {
  def load(key: LocalFeatureCacheKey): FeatureVector = {
    key match {
      case LabelKey(head, mod, typ) => synFactory.createLabelFeatures(inst, head, mod, typ)
      case ArcKey(head, mod) => synFactory.createArcFeatures(inst, mod, head);
      case NeighborKey(par, h, left, right) =>
        synFactory.createNeighborFeatureVector(inst, par, h, left, right)
      case PPKey(gp, h, m) => synFactory.createPPFeatureVector(inst, gp, h, m)
      case CC1Key(left, arg, right) => synFactory.createCC1FeatureVector(inst, left, arg, right)
      case _ => ???
    }
  }
}

class IncScoreCacheLoader(var lfd: IncLocalFeatureData)
  extends CacheLoader[LocalFeatureCacheKey, java.lang.Double] {

  def load(key: LocalFeatureCacheKey): java.lang.Double = key match {
    case ArcKey(head, mod) => ???
      case TripsKey(h, m, s) =>  lfd.parameters.dotProduct(lfd.getTripsFeatureVector(h, m, s)) * lfd.gamma
      case SibKey(m, s) => lfd.parameters.dotProduct(lfd.getSibFeatureVector(m, s)) * lfd.gamma
      case GPCKey(gp, h, m) => lfd.parameters.dotProduct(lfd.getGPCFeatureVector(gp, h, m)) * lfd.gamma
      case HeadBiKey(h, m, h2) => lfd.parameters.dotProduct(lfd.getHeadBiFeatureVector(h, m, h2)) * lfd.gamma
      case GPSibKey(gp, h, m, s) => lfd.parameters.dotProduct(lfd.getGPSibFeatureVector(gp, h, m, s)) * lfd.gamma
      case TriSibKey(h, m, s1, s2) => lfd.parameters.dotProduct(lfd.getTriSibFeatureVector(h, m, s1, s2)) * lfd.gamma
      case GGPCKey(ggp, gp, h, m) => lfd.parameters.dotProduct(lfd.getGGPCFeatureVector(ggp, gp, h, m)) * lfd.gamma
      case PSCKey(h, m, c, sib) => lfd.parameters.dotProduct(lfd.getPSCFeatureVector(h, m, c, sib)) * lfd.gamma
    case NeighborKey(par, h, left, right) =>
      lfd.parameters.dotProduct(lfd.gfd.getNeighborFeatureVector(par, h, left, right)) * lfd.gamma
    case PPKey(gp, h, m) => lfd.parameters.dotProduct(lfd.gfd.getPPFeatureVector(gp, h, m)) * lfd.gamma
    case CC1Key(left, arg, right) => lfd.parameters.dotProduct(lfd.gfd.getCC1FeatureVector(left, arg, right)) * lfd.gamma
    case CC2Key(arg, head, child) => lfd.parameters.dotProduct(lfd.gfd.getCC2FeatureVector(arg, head, child)) * lfd.gamma
    case PNXKey(head, arg, pair) =>  lfd.parameters.dotProduct(lfd.gfd.getPNXFeatureVector(head, arg, pair)) * lfd.gamma
    case SpanKey(h, end, punc, bin) => lfd.parameters.dotProduct(lfd.gfd.getSpanFeatureVector(h, end, punc, bin)) * lfd.gamma
    case CNKey(h, leftNum, rightNum) => lfd.parameters.dotProduct(lfd.gfd.getChildNumFeatureVector(h, leftNum, rightNum)) * lfd.gamma
    case LabelKey(_,_,_) => ??? // label scores are not cached
  }
}

class IncLocalFeatureData(_inst: DependencyInstance, parser: DependencyParser,
                          indexGoldArgs:  Boolean, val fvcache: LoadingCache[LocalFeatureCacheKey, FeatureVector],
                          val scoreCache: LoadingCache[LocalFeatureCacheKey, java.lang.Double],
                          val lastNormalWord: Int)
    extends LocalFeatureData(_inst, parser, indexGoldArgs) {

  // Cache for everything touching the VNs (not cacheable betweeen IncLocalFeatureData instances
  val localScoreCache: LoadingCache[LocalFeatureCacheKey, java.lang.Double] = Caffeine.newBuilder().
    maximumSize(5000).
    // recordStats().
    build[LocalFeatureCacheKey, java.lang.Double](new IncScoreCacheLoader(this))

  // note to self: It doesn't make sense to cache FeatureVectors containing virtual nodes as they
  // can't be cached between increments.  Since the scores are cached already, they should only be requested once.

  val gfd = new IncGlobalFeatureData(this)

  override def getLabelFeature(head: Int, mod: Int, typ: Int): FeatureVector = {
    if (head <= lastNormalWord && mod <= lastNormalWord)
      fvcache.get(LabelKey(head,mod,typ))
    else
      pipe.synFactory.createLabelFeatures(inst, head, mod, typ)
  }

  override def getArcFeatureVector(h: Int, m: Int): FeatureVector = {
    if (h <= lastNormalWord && m <= lastNormalWord)
      fvcache.get(ArcKey(h, m))
    else
      pipe.synFactory.createArcFeatures(inst, h, m)
  }

  override def getArcScore(h: Int, m: Int): Double = {
    if (arcScores(m + h*len) != LocalFeatureData.NULL) {
      return arcScores(m + h*len)
    }
    val arcFv = getArcFeatureVector(h, m)
    val score = parameters.dotProduct(arcFv) * gamma + parameters.dotProduct(wpU(h), wpV(m), h-m) * (1-gamma)
    arcScores(m + h*len) = score
    score
  }

  // We don't want to cache scores and features here, therefore
  // - don't pre-compute Arc Features and scores (most are already
  //   computed & in the cache or not needed at all)
  // - don't use the higher order score tables as simply creating them is costly.
  override def initFirstOrderTables() = {
    wordFvs = new Array[FeatureVector](len)
	wpU = Array.ofDim[Double](len, rank)
	wpV = Array.ofDim[Double](len, rank)

    arcScores = new Array[Double](len*len)
    java.util.Arrays.fill(arcScores, LocalFeatureData.NULL)
    var i = 0
	while (i < len) {
	  wordFvs(i) = pipe.synFactory.createWordFeatures(inst, i)
	  //wpU[i] = parameters.projectU(wordFvs[i]);
	  //wpV[i] = parameters.projectV(wordFvs[i]);
	  parameters.projectU(wordFvs(i), wpU(i))
	  parameters.projectV(wordFvs(i), wpV(i))
    i+=1
	}
  }

  override def initHighOrderFeatureTables() = {}

  // TripsFeatureVector calls addTurboSib, which looks 1 into the future
  override def getTripsScore(h: Int, m: Int, s: Int): Double = {
    val c = if (h <= lastNormalWord && m < lastNormalWord && s < lastNormalWord) scoreCache else localScoreCache
    c.get(TripsKey(h,m,s))
  }

  override def getSibScore(m: Int, s: Int): Double = {
    val c = if (m <= lastNormalWord && s <= lastNormalWord) scoreCache else localScoreCache
    c.get(SibKey(m, s))
  }

  // uses addTurboGPC which looks 1 into the future
  override def getGPCScore(gp: Int, h: Int, m: Int): Double = {
    val c = if (gp < lastNormalWord && h < lastNormalWord && m < lastNormalWord) scoreCache else localScoreCache
    c.get(GPCKey(gp, h, m))
  }

  //createHeadBiFeaturevector looks 1 into future for ch (first argument), but first and second are switched
  // between getHeadBiFeatureVector and createHeadBiFeatureVector!
  override def getHeadBiScore(h: Int, m: Int, h2: Int): Double = {
    val c = if (h <= lastNormalWord && m < lastNormalWord && h2 <= lastNormalWord) scoreCache else localScoreCache
    c.get(HeadBiKey(h, m, h2))
  }

  override def getGPSibScore(gp: Int, h: Int, m: Int, s: Int): Double = {
    val c = if (gp <= lastNormalWord && h <= lastNormalWord && m <= lastNormalWord && s <= lastNormalWord) scoreCache else localScoreCache
    c.get(GPSibKey(gp, h, m, s))
  }

  override def getTriSibScore(h: Int, s1: Int, m: Int, s2: Int): Double = {
    val c = if (h <= lastNormalWord && s1 <= lastNormalWord && m <= lastNormalWord && s2 <= lastNormalWord) scoreCache else localScoreCache
    c.get(TriSibKey(h, s1, m, s2))
  }

  override def getGGPCScore(ggp: Int, gp: Int, h: Int, m: Int): Double = {
    val c = if (ggp <= lastNormalWord && gp <= lastNormalWord && h <= lastNormalWord && m <= lastNormalWord) scoreCache else localScoreCache
    c.get(GGPCKey(ggp, gp, h, m))
  }

  override def getPSCScore(h: Int, m: Int, c: Int, sib: Int): Double = {
    val cache = if (h <= lastNormalWord && m <= lastNormalWord && c <= lastNormalWord && sib <= lastNormalWord) scoreCache else localScoreCache
    cache.get(PSCKey(h, m, c, sib))
  }
}


class IncGlobalFeatureData(ilfd: IncLocalFeatureData) extends GlobalFeatureData(ilfd) {
  val lastNormalWord = ilfd.lastNormalWord
  val cache = ilfd.fvcache
  val scoreCache = ilfd.scoreCache
  val localScoreCache = ilfd.localScoreCache

  // we don't cache our values in arrays
  override protected def initArrays() = {
    // init array not necessary because we use a cache instead
  }

  override def getNeighborFeatureVector(par: Int, h: Int, left: Int, right: Int): FeatureVector = {
    // par&h are word indices, left and right PoS tags
    if (par <=lastNormalWord && h <= lastNormalWord)
      cache.get(NeighborKey(par, h, left, right))
     else synFactory.createNeighborFeatureVector(lfd.inst, par, h, left, right)
  }

  override def getNeighborScore(par: Int, h: Int, left: Int, right: Int) = {
    val c = if (h <= lastNormalWord && par <= lastNormalWord) scoreCache else localScoreCache
    c.get(NeighborKey(par, h, left, right))
  }

  override def getPPScore(gp: Int, h: Int, m: Int): Double = {
    val c = if (gp <= lastNormalWord && h <= lastNormalWord && m <= lastNormalWord) scoreCache else localScoreCache
    c.get(PPKey(gp, h, m))
  }

  override def getPPFeatureVector(gp: Int, h: Int, m: Int): FeatureVector = {
    if (gp <= lastNormalWord && h <= lastNormalWord && m <= lastNormalWord)
      cache.get(PPKey(gp, h, m))
    else
      synFactory.createPPFeatureVector(lfd.inst, gp, h, m)
  }

  override def getCC1Score(left: Int, arg: Int, right: Int) = {
    val c = if (left <=lastNormalWord && arg <= lastNormalWord && right <= lastNormalWord) scoreCache else localScoreCache
    c.get(CC1Key(left, arg, right))
  }

  override def getCC1FeatureVector(left: Int, arg: Int, right: Int): FeatureVector = {
    if (left <=lastNormalWord && arg <= lastNormalWord && right <= lastNormalWord)
      cache.get(CC1Key(left, arg, right))
    else
      synFactory.createCC1FeatureVector(lfd.inst, left, arg, right)
  }

  override def getCC2Score(arg: Int, head: Int, child: Int) = {
    val c = if (arg <= lastNormalWord && head <= lastNormalWord && child <= lastNormalWord) scoreCache else localScoreCache
    c.get(CC2Key(arg, head, child))
  }

  override def getPNXScore(head: Int, arg: Int, pair: Int) = {
    val c = if (head <= lastNormalWord && arg <= lastNormalWord) scoreCache else localScoreCache
    c.get(PNXKey(head, arg, pair))
  }

  override def getSpanScore(h: Int, end: Int, punc: Int, bin: Int) = {
    val c = if (h <= lastNormalWord) scoreCache else localScoreCache
    c.get(SpanKey(h, end, punc, bin))
  }

  override def getChildNumScore(h: Int, leftNum: Int, rightNum: Int): Double = {
    val c = if (h <= lastNormalWord) scoreCache else localScoreCache
    c.get(CNKey(h, leftNum, rightNum))
  }

}

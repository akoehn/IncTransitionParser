package parser

import java.io.{File, FileOutputStream, FileWriter}

import parser.io.Conll06Reader

// import parser.InstanceData

/* for REPL only

 import parser.TransitionParser
 import parser.decoding.TransitionState
 import collection.JavaConversions._

 val p = new TransitionParser(10)

 var currStates: Seq[TransitionState] = List(new TransitionState(List(), List()))
 val initState: TransitionState = (new TransitionState(List(), List())).extendWords(List("Peter"), List("NE"))

  p.produce_final_states(Set(initState))

 */


/*
 * unlabeled: number of attachments to the wrong word
 * labeled: unlabeled + attachments with wrong label
 * predicted: attachments to VN1 if gold is attached to VN2 (a subset of unlabeled errors)
 */
/* sealed case class AttachmentErrors(uprefix: Integer, lprefix: Integer, upredicted: Integer, lpredicted: Integer, notpredicted: Integer) {
  def labeled = lpredicted + lprefix + notpredicted
  def unlabeled = upredicted + uprefix + notpredicted
}

class TrainStruct[T<:BaseState[T]](val state: T, val score: Double) {
  var numErrors = AttachmentErrors(-1,-1,-1,-1,-1)
  var loss: Double = -1
}

class TrainStructRef[T<: BaseState[T]]() {
  var ts: TrainStruct[T] = null
  def :=(ts1: TrainStruct[T]) = {ts = ts1}
}

object TrainStructRef {
  implicit def TrainStructRefToTrainStruct[T<: BaseState[T]](tsr: TrainStructRef[T]): TrainStruct[T] = tsr.ts
}
// implicit def doubleToMyComplex(d: Double) = new MyComplex(d,0.)
*/

/**
 * Created by Arne Köhn on 11/21/14.
 * This is currently an experimental main method
 */


object main {
  def parseArgs(args: Array[String]): ParserConf = {
    new ParserConf(args)
  }
 // val featureCache: mutable.HashMap[String, (LocalFeatureData, GlobalFeatureData)] = new mutable.HashMap[String, Tuple2[LocalFeatureData, GlobalFeatureData]]()
/*
  List(10,50,100,300).foreach((beamsize) => {
    val p = new TransitionParser(beamsize/*, featureCache*/)
    //  p.parse(List("Peter", "isst", "ein", "Eis"), List("NE", "VVFIN", "DET", "NN"), List("SUBJ", "S", "DET", "OBJA"))
    //p.parse(List("Menschen", "essen", "ein", "Eis"), List("NN", "VVFIN", "DET", "NN"), List("", "", "", ""))
    val inst = p.parse(List("Altersteilszeit-Regelungen", "gab", "es", "in", "zehn", "Branchen", "."),
      List("_", "_", "_", "_", "_", "_", "_"),
      List("Altersteilszeit-Regelung", "geben", "es", "in", "zehn", "Branche", "--"))
/*
    val args2 = Array("train", "label:false", "model-file:/tmp/transition.model", "train-file:/tmp/rbgtest",
    "model:full", "thread:12")
    val options: Options = new Options()
    options.processArguments(args2)
    val read = new CONLLReader(options)
    read.startReading("/tmp/foobar.conll")
*/
*/
    // val p = new ShiftBasedTransitionParser(10)
    // p.train("/informatik2/nats/home/koehn/tmpdata/foobar.conll")

  def main(args: Array[String]) : Unit = {
    val opts = parseArgs(args)
    if (opts.help()) {
      opts.printHelp()
      System.exit(0)
    }
    println(opts.summary)
    if (opts.train() || opts.test() || opts.testHillClimbing()) {
      val p = new TAGLikeTransitionParser(opts)
      if (opts.train())
        p.train(opts.trainFile())
      if (opts.test())
        p.test()
      if (opts.testHillClimbing())
        p.testWithIncHillClimbingDecoder()
    }
    if (opts.evaluate()) {
      val esc = new ErrorStatsCollector(opts.evalFrontierSize())
      val sr = new Conll06Reader()
      sr.startReading(opts.evalSystemFile())
      val gr = new Conll06Reader()
      gr.startReading(opts.evalGoldFile())
      esc.evalConll(sr, gr)
      if (opts.evalOutputFile.isSupplied) {
        val f = new FileWriter(new File(opts.evalOutputFile()))
        f.write(esc.rstring)
        f.write("\n")
        f.close()
      } else {
        print(esc.rstring)
      }
      // stability
      val escs = new ErrorStatsCollector(opts.evalFrontierSize())
      val srs = new Conll06Reader()
      srs.startReading(opts.evalSystemFile())
      val grs = new Conll06Reader()
      grs.startReading(opts.evalSystemFile())
      escs.evalConll(srs, grs, true)
      if (opts.evalOutputFile.isSupplied) {
        val f = new FileWriter(new File(opts.evalOutputFile() + ".stability"))
        f.write(escs.rstring)
        f.write("\n")
        f.close()
      } else {
        print("stability:\n")
        print(escs.rstring)
      }
    }
  }
}

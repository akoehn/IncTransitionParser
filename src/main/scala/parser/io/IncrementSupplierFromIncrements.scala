package parser.io

import parser.{AbstractTransitionParser, DependencyInstance, Options}

class IncrementSupplierFromIncrements(override val options: Options, override val fname: String) extends IncrementSupplier(options, fname) {

  val _nextInstance: DependencyInstance = reader.nextInstance()
  var nextResult = (_nextInstance, AbstractTransitionParser.lastNormalWord(_nextInstance.forms), true)

  override def hasNext: Boolean = nextResult != null

  override def next(): (DependencyInstance, Int, Boolean) = {
    val res = nextResult
    val nextInstance = reader.nextInstance()
    if (nextInstance == null)
      nextResult = null
    else {
      val nextLastNormalWord = AbstractTransitionParser.lastNormalWord(nextInstance.forms)
      nextInstance.lastNormalWord = nextLastNormalWord
      nextResult = (nextInstance, nextLastNormalWord, res._2 >= nextLastNormalWord)
    }
    res
  }
}

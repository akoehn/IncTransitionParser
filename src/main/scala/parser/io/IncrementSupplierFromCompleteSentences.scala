package parser.io

import parser.{DependencyInstance, Options}

class IncrementSupplierFromCompleteSentences(opts:Options, fname: String) extends IncrementSupplier(opts, fname) {
  var currSentence: DependencyInstance = reader.nextInstance()
  var nextSentence: DependencyInstance = reader.nextInstance()

  var currSentenceLength: Int = currSentence.forms.length - 1
  var currIndex = 0

  override def hasNext: Boolean = nextSentence != null || currIndex < currSentenceLength

  override def next(): (DependencyInstance, Int, Boolean) = {
    currIndex += 1
    val newSentence = currSentenceLength < currIndex
    if (newSentence) {
      currSentence = nextSentence
      nextSentence = reader.nextInstance()
      currSentenceLength = currSentence.forms.length -1
      currIndex = 1
    }
    (currSentence, currIndex, newSentence)
  }
}

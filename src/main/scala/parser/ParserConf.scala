package parser

import org.rogach.scallop.ScallopConf
import parser.scoring.LstmConf

/**
  * Created by arne on 09.11.16.
  */
class ParserConf(args: Seq[String]) extends ScallopConf(args) {
  guessOptionName = true
  val help = opt[Boolean](default = Some(false))
  // For Training and testing
  val beamSize = opt[Int](default = Some(10), validate = 0 < _, descr = "Beam size")
  val modelFile = opt[String](descr = "Model to start training from")
  val rbgOptions = opt[List[String]](default=Some(List[String]()), descr = "Options passed to RBGParser proper. Format: key:value")
  val numThreads = opt[Int](default=Some(Runtime.getRuntime().availableProcessors()), descr="Number of iterations through train file")
  val maxNumVirtualNodes = opt[Int](default=Some(2), descr="Maximal number of virtual nodes in an analysis")
  val htmlErrorLog = opt[Boolean](default = Some(false), descr="write debug html logs for transition system errors")
  val htmlTrace =  opt[Boolean](default = Some(false), descr="write debug html logs detailing every transition")
  val unstablePredictions = toggle(default = Some(true), descrYes="revert virtual nodes without non-virtual dependents before parsing next increment")
  val noTopDownPredictions = opt[Boolean](default = Some(false), descr="don't predict additional nodes without dependends.")
  val inputIsIncrements = opt[Boolean](default = Some(false), descr = "whether the input is incremental (with predictions) or complete sentences")
  val useStandardScoring = toggle(default = Some(true), descrYes = "use RBGParser scoring")

  // input filtering
  val filterPos =  opt[Boolean](default = Some(false), descr="remove pos tags from input")
  val filterMorph =  opt[Boolean](default = Some(false), descr="remove feature column from input")
  val filterLemma =  opt[Boolean](default = Some(false), descr="remove lemma from input")

  //training
  val train = opt[Boolean](default = Some(false))
  val trainHillClimbing = opt[Boolean](default = Some(false))
  val trainLstmOnly = opt[Boolean](default = Some(false), descr = "Use a pre-trained model for the standard scorer and only train" +
    "the lstm part")
  val trainFile = opt[String](descr = "CoNLL file to train on")
  val validationFile = opt[String](descr = "validation file to determine best model")
  val outModelFile = opt[String](descr = "Where to store a trained model")
  val numIterations = opt[Int](default=Some(1), descr="Number of iterations through train file")
  val augmentLoss = opt[Boolean](default = Some(false), descr = "add errors to score during training")
  val writeIterationModels = opt[Boolean](default = Some(false),
                                          descr = "write a model after each iteration during training. " +
                                                   "\".it-X\" will be appended to the model name.")
  val updateOncePerSentences = opt[Boolean](default = Some(false), descr = "only perform training update once for each sentence" +
    "and disable updates for the rest of the sentence.")
  val goldAlwaysInTest = opt[Boolean](default = Some(false), descr = "Add Gold standard tree to test stream to simulate perfect beam")

  // test
  val test = opt[Boolean](default = Some(false))
  val testHillClimbing = opt[Boolean](default = Some(false))
  val testFile = opt[String](descr = "CoNLL file to test on")
  val outFile =  opt[String](descr = "CoNLL output file")
  val testSkipEvaluation = opt[Boolean](default = Some(false), descr = "don't evaluate against the gold standard during testing")

  // evaluate
  val evaluate = opt[Boolean](default = Some(false))
  val evalSystemFile = opt[String](descr = "CoNLL file with increments")
  val evalGoldFile = opt[String](descr = "CoNLL file with gold standards")
  val evalOutputFile = opt[String](descr = "Where to write results to.  If not provided, stdout is used.")

  val lstmSubcommand = new LstmConf("--lstm-opts")
  addSubcommand(lstmSubcommand)
  // Evaluation
  val evalFrontierSize = opt[Int](default=Some(6), descr="Number of elements in the evaluation frontier")
  // optimization flags
  // none yet
  dependsOnAll(train, List(trainFile, outModelFile))
  dependsOnAll(test, List(modelFile, testFile))
  dependsOnAll(testHillClimbing, List(modelFile, testFile))
  dependsOnAll(trainHillClimbing, List(train))
  dependsOnAll(trainLstmOnly, List(train, modelFile))
  dependsOnAll(evaluate, List(evalSystemFile, evalGoldFile))
  conflicts(testSkipEvaluation, List(train, evaluate))
  verify()
}

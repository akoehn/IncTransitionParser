package parser

import com.typesafe.scalalogging.Logger
import org.eclipse.collections.api.block.predicate.primitive.IntPredicate
import org.eclipse.collections.api.list.primitive.ImmutableIntList
import org.slf4j.LoggerFactory
import parser.AbstractTransitionParser.VN_UNMAPPED
import parser.decoding.BaseState
import parser.evaluation.Matching
import parser.io.Conll06Reader

import scala.collection.mutable.ArrayBuffer


object ErrorStatsCollector {
  val logger = Logger(LoggerFactory.getLogger("ErrorStatsCollector"))
  def computeErrorsWithMatch(predicted: DependencyInstance, goldInstance: DependencyInstance,
                             lastNormalWord: Int, bmatch: Array[Int]): AttachmentErrors = {
    logger.debug("computing errors on " + predicted)
    var uprefix = 0
    var lprefix = 0
    var upredicted = 0
    var lpredicted = 0

    // First, count in prefix
    (1 to lastNormalWord) foreach((i) => {
      val goldHead = goldInstance.heads(i)
      if (bmatch(predicted.heads(i)) != goldHead) {
        uprefix += 1
        lprefix += 1
      } else if (predicted.deprels(i) != goldInstance.deprels(bmatch(i))) {
        lprefix += 1
      }
    })

    // and again for predicted words
    (lastNormalWord + 1 to bmatch.length-1) foreach((i) => {
      val goldHead = if (bmatch(i) == AbstractTransitionParser.VN_UNMAPPED) -987 else goldInstance.heads(bmatch(i))
      if (bmatch(predicted.heads(i)) != goldHead) {
        upredicted += 1
        lpredicted += 1
      } else if (predicted.deprels(i) != goldInstance.deprels(bmatch(i))) {
        lpredicted += 1
      }
    })

    // last: The number of VNs that are in gold but not in predicted

    val notpredicted = goldInstance.heads.length - bmatch.count((value: Int) => value != VN_UNMAPPED)

    return AttachmentErrors(uprefix, lprefix, upredicted, lpredicted, notpredicted)
  }
}

class ErrorStatsCollector[T<:BaseState[T]](val frontierSize: Int) {
  import ErrorStatsCollector._

  class ErrorTypes {
    var correct_in_prefix = 0
    var correct_prediction = 0
    // incorrect and either gold or sytem output is in prefix
    var incorrect_in_prefix = 0
    // incorrect and both system and gold are heads outside prefix
    var incorrect_prediction = 0

    def total = correct_in_prefix + correct_prediction + incorrect_in_prefix + incorrect_prediction
    def correct = correct_in_prefix + correct_prediction
    def incorrect = incorrect_in_prefix + incorrect_prediction

  }

  var nonIncErrorsUnlabeled = 0
  var nonIncErrorsLabeled = 0
  var nonIncTokensOverall = 0



  // The number of errors seen in each frontier element
  val frontierErrors = new Array[ErrorTypes](frontierSize)
  val frontierErrorsLabeled = new Array[ErrorTypes](frontierSize)
  for (i <- 0 until frontierSize) {
    frontierErrors(i) = new ErrorTypes()
    frontierErrorsLabeled(i) = new ErrorTypes()
  }
  // The number of elements seen in each frontier element
  // val frontierNum = new Array[Int](frontierSize)

  var vnsPredicted = 0
  var vnsCorrectlyPredicted = 0
  var vnsInGold = 0

  // to count overall errors
  var numHeads = 0
  var numRealHeads = 0
  var numErrors: AttachmentErrors = EmptyErrors

  def gnuplotString: String = {
    val res = new StringBuilder()
    res ++= "# id total unspec cor_u cor_l stab_u stab_l nonspec ns_c_u ns_c_l\n"
    for (i <- frontierErrors.indices) {
      res.append(i)
        .append(" ")
        .append(frontierErrors(i).total)
        .append(" 0 ")
        .append(frontierErrors(i).correct)
        .append(" ")
        .append(frontierErrorsLabeled(i).correct)
        .append(" 0 0 0 ")
        .append(frontierErrors(i).correct_prediction)
        .append(" ")
        .append(frontierErrorsLabeled(i).correct_prediction)
        .append("\n")
    }
    res.toString()
  }

  def rstring: String = {
    val res = new StringBuilder()
    res ++= "frontier corrip corrpred incorrpred incorr corripl corrpredl incorrpredl incorrl\n"
    for (i <- frontierErrors.indices) {
      res.append(i)
        for (etype <- List(frontierErrors, frontierErrorsLabeled)) {
          res.append(" ")
            .append(etype(i).correct_in_prefix)
            .append(" ")
            .append(etype(i).correct_prediction)
            .append(" ")
            .append(etype(i).incorrect_prediction)
            .append(" ")
            .append(etype(i).incorrect_in_prefix)
        }
        res.append("\n")
    }
    res.append(s"${frontierErrors.length} ${nonIncTokensOverall-nonIncErrorsUnlabeled} 0 0 ${nonIncErrorsUnlabeled} ")
    res.append(s"${nonIncTokensOverall-nonIncErrorsLabeled} 0 0 ${nonIncErrorsLabeled}\n")
    res.append(s"# num predictions: $vnsPredicted  num correct predictions: $vnsCorrectlyPredicted\n")
    res.toString()
  }

  /**
    *
    * @param predictedStruct
    */
  private def count_overall_stats(predictedStruct: TrainStruct[T]) = {
    numHeads += predictedStruct.state.getHeads.length - 1
    numRealHeads += predictedStruct.state.lastWord
    numErrors += predictedStruct.numErrors
  }

  @inline
  def labelIdentical(di1: DependencyInstance, di2: DependencyInstance, index: Int): Boolean = {
    (di1.deplbids == null && di1.deprels(index).equals(di2.deprels(index))) ||
      (di1.deplbids != null && di1.deplbids(index).equals(di2.deplbids(index)))
  }

  def addToStats(predictedStruct: TrainStruct[T], gold: DependencyInstance,
                 lastNormalWord: Int, bmatch: Array[Int]): Unit = {
    count_overall_stats(predictedStruct)
    addToStats(predictedStruct.state.getInstance, gold, lastNormalWord, bmatch)
  }

    def addToStats(predicted: DependencyInstance, gold: DependencyInstance,
                   lastNormalWord: Int, bmatch: Array[Int]): Unit = {

      // iterate over frontier to see whether each attachment is correct
      // we go from the newest word (i==0, index == lastNormalWord)
      // to either the frontierSize-newest word or to the first word of
      // the prefix, whichever is nearer the the newest word.
      0 until math.min(frontierSize, lastNormalWord-1) foreach (i => {
        val index = lastNormalWord - i
        // frontierNum(i) += 1

        if (predicted.heads(index) <= lastNormalWord || gold.heads(index) <= lastNormalWord) {
          if (bmatch(predicted.heads(index)) == gold.heads(index)) {
            frontierErrors(i).correct_in_prefix += 1
            if (labelIdentical(predicted, gold, index)) {
              frontierErrorsLabeled(i).correct_in_prefix += 1
            } else {
              frontierErrorsLabeled(i).incorrect_in_prefix += 1
            }
          } else {
            frontierErrors(i).incorrect_in_prefix += 1
            frontierErrorsLabeled(i).incorrect_in_prefix += 1
          }
        } else {
          // prediction errors
          if (bmatch(predicted.heads(index)) == gold.heads(index)) {
            frontierErrors(i).correct_prediction += 1
            if (labelIdentical(predicted, gold, index)) {
              frontierErrorsLabeled(i).correct_prediction += 1
            } else {
              frontierErrorsLabeled(i).incorrect_prediction += 1
            }
          } else {
            frontierErrors(i).incorrect_prediction += 1
            frontierErrorsLabeled(i).incorrect_prediction += 1
          }
        }
      })
      // count correct prediction nodes
      lastNormalWord+1 until predicted.length foreach (i =>{
        vnsPredicted += 1
        val matchedToken = bmatch(i)
        if (matchedToken > 0 && bmatch(predicted.heads(i)) == gold.heads(matchedToken)) {
          vnsCorrectlyPredicted += 1
        }
      })
  }

  def printStats() = {
    println("Unlabeled and labeled accuracies for Frontier:")
    for (i <- 0 until frontierSize) {
      print(s"$i: ")
      for (eType <- List(frontierErrors, frontierErrorsLabeled)) {
        print(f" ${(eType(i).correct.floatValue() / eType(i).total)*100}%2.2f")
      }
      println()
    }
    println(s"prediction nodes predicted: $vnsPredicted")
    println(s"prediction nodes correct: $vnsCorrectlyPredicted")

    logger.info(s"Average Error: ${numErrors.unlabeled.toFloat / numHeads} " +
        s"avg error in prefix: ${numErrors.uprefix.toFloat / numRealHeads}" +
        s"avg withouth nonpredicted: ${(numErrors.unlabeled.toFloat - numErrors.notpredicted) / numHeads}" +
        s"\n raw: ${numHeads}, ${numErrors}")
  }

  def evalConll(systemOutput: Conll06Reader, goldStandard: Conll06Reader, computeStability: Boolean = false, gsIsInc: Boolean = false) = {
    val sit = new SentenceIterator(systemOutput)
    val git: Iterator[Seq[DependencyInstance]] = if (gsIsInc) new SentenceIterator(goldStandard) else if (computeStability) new StabilitySentenceIterator(goldStandard) else null
    for (sent <- sit) {
      val gold = if (git != null) git.next() else List[DependencyInstance]().padTo(sent.length, goldStandard.nextInstance())
      // full-sentence accuracy needs to be computed on the last element of this
      // iterator.  Reverse so it is the first one.
      val s_g_it = sent.zip(gold).reverse
      val (s,g) = s_g_it.take(1).head
      val bm = Matching.bestMatch(g.heads, s.heads, s.lastNormalWord)
      this.addToStats(s, g, s.lastNormalWord, bm)
      this.addNonIncErrors(g, s, bm)

      for ((s,g) <- s_g_it) {
        val bm = Matching.bestMatch(g.heads, s.heads, s.lastNormalWord)
        this.addToStats(s, g, s.lastNormalWord, bm)
      }
    }
  }

  def addNonIncErrors(gold: DependencyInstance,
                predicted: DependencyInstance,
                pmatch: Array[Int]) = {
    var i = 1; while (i < gold.length) {
      nonIncTokensOverall += 1
      if (pmatch(predicted.heads(i)) != gold.heads(i)) {
        nonIncErrorsUnlabeled += 1
        nonIncErrorsLabeled += 1
      } else {
        if (!labelIdentical(predicted, gold, i))
          nonIncErrorsLabeled += 1
      }
      i += 1
    }
  }

  class SentenceIterator(r: Conll06Reader) extends Iterator[Seq[DependencyInstance]] {
    private var nextIncrement = r.nextInstance()

    override def hasNext: Boolean = nextIncrement != null

    override def next(): Seq[DependencyInstance] = {
      val res = new ArrayBuffer[DependencyInstance]()
      var lastLength = 0
      while (nextIncrement != null && nextIncrement.lastNormalWord > lastLength) {
        res.append(nextIncrement)
        lastLength = nextIncrement.lastNormalWord
        nextIncrement = r.nextInstance()
      }
      res
    }
  }

  //
  class StabilitySentenceIterator(r: Conll06Reader) extends Iterator[Seq[DependencyInstance]] {
    private var nextIncrement = r.nextInstance()

    override def hasNext: Boolean = nextIncrement != null

    override def next(): Seq[DependencyInstance] = {
      val res = new ArrayBuffer[DependencyInstance]()
      var lastLength = 0
      while (nextIncrement != null && nextIncrement.lastNormalWord > lastLength) {
        res.append(nextIncrement)
        lastLength = nextIncrement.lastNormalWord
        nextIncrement = r.nextInstance()
      }
      val fullSentenceInstance = res.last
      val numElements = res.length
      res.clear()
      res.padTo(numElements, fullSentenceInstance)
    }
  }
}

/* package parser.decoding

import com.google.common.base.Joiner
import com.google.common.collect.Lists
import gnu.trove.list.array.TIntArrayList
import gnu.trove.map.hash.THashMap
import gnu.trove.set.hash.THashSet
import parser.DependencyInstance
import parser.DependencyParser
import java.util._

/**
  * Created by Arne Köhn on 10/22/14.
  */
object TransitionState {
  val MAX_NUM_VNS: Int = 2
  val VN_NAME: String = "[virtual]"

  //"*UNKNOWN*";
  object ReduceType extends Enumeration {
    type ReduceType = Value
    val LEFT, RIGHT, TWOLEFT, TWORIGHT, ROOT = Value
  }

  object action_types extends Enumeration {
    type action_types = Value
    val SHIFT, ADDVN, ADDWORD, LEFT, RIGHT, TWOLEFT, TWORIGHT, ROOT, INIT = Value
  }

  def emptyState: TransitionState = {
    return new TransitionState(new util.ArrayList[String], new util.ArrayList[String], new util.ArrayList[String])
  }

  def getEmptyState: TransitionState = {
    return new TransitionState(new util.ArrayList[_], new util.ArrayList[_], new util.ArrayList[_])
  }
}

class TransitionState extends BaseState {
  type T = TransitionState
  var heads: Array[Int] = null
  var stack: util.List[Integer] = null
  var input: util.List[Integer] = null
  var words: util.List[String] = null
  var lemmas: util.List[String] = null
  var pos: util.List[String] = null
  /** TODO! deprel not fully implemented yet */
  var deprel: Array[String] = null
  var previousState: TransitionState = null
  /** whether a root is already present **/
  var hasRoot: Boolean = false
  var num_vns: Int = 0
  var score: Double = .0
  var action: TransitionState.action_types = null
  /** whether a VN has been part of an attachment
    * important for ordering since we want to create those states last
    */
  var VN_interaction_has_happened: Boolean = false
  // Pointer to where to roll back to if a new word appears
  var rollBackTo: TransitionState = null
  var instance: DependencyInstance = null

  // def this(words: util.List[String], pos: util.List[String], lemmas: util.List[String])

  def this(previousState: TransitionState) {
    this()
    this.num_vns = previousState.num_vns
    this.VN_interaction_has_happened = previousState.VN_interaction_has_happened
    this.hasRoot = previousState.hasRoot
    this.heads = previousState.heads.clone
    this.stack = new util.ArrayList[Integer](previousState.stack)
    this.input = new util.ArrayList[Integer](previousState.input)
    // Words don't change and don't need copying
    this.words = previousState.words
    this.lemmas = previousState.lemmas
    this.pos = previousState.pos
    this.deprel = previousState.deprel
    this.previousState = previousState
    // by default, roll back to the same, needs to be overwritten
    // somewhere else.
    this.rollBackTo = previousState.rollBackTo
  }

  def getHeads: Array[Int] = {
    return heads
  }

  def getDeprel: Array[String] = {
    return deprel
  }

  def getScore: Double = {
    return score
  }

  def setScore(newScore: Double) {
    score = newScore
  }

  /**
    * Returns an uninstantiated(!) instance. For testing purposes.
    */
  private var _instanceUninst: DependencyInstance = null

  def toInstanceUninstantiated: DependencyInstance = {
    //(String[] forms, String[] lemmas, String[] cpostags, String[] postags,
    //       String[][] feats, int[] heads, String[] deprels)
    if (_instanceUninst != null) return _instanceUninst
    val wordarr: Array[String] = words.toArray(new Array[String](words.size))
    val lemmasarr: Array[String] = lemmas.toArray(new Array[String](lemmas.size))
    val posarr: Array[String] = pos.toArray(new Array[String](pos.size))
    // TODO Handle cpos
    val cposarr: Array[String] = posarr
    //for (int i = 0; i< cposarr.length; i++)
    //    cposarr[i] = parser.pipe.coarseMap.get(posarr[i]);
    // no features as of now
    val feats: Array[Array[String]] = new Array[Array[String]](words.size, 0)
    _instanceUninst = new DependencyInstance(wordarr, lemmasarr, cposarr, posarr, feats, heads, deprel)
    return _instanceUninst
  }

  def toInstance(parser: DependencyParser): DependencyInstance = {
    if (instance != null) return instance
    val result: DependencyInstance = toInstanceUninstantiated
    result.setInstIds(parser)
    // cache for future invocations
    instance = result
    return result
  }

  def getInstance: DependencyInstance = {
    return instance
  }

  /**
    * TODO proper implementation?
    *
    * @param words
    * @param pos
    * @param lemmas
    * @return
    */
  def includeNewWord(words: util.List[_], pos: util.List[_], lemmas: util.List[_]): util.Set[_] = {
    val result: util.HashSet[TransitionState] = new util.HashSet[TransitionState]
    result.add(extendWords(words, pos, lemmas))
    return result
  }

  def extendWords(words: util.List[String], pos: util.List[String], lemmas: util.List[String]): TransitionState = {
    assert(words.size == pos.size)
    assert(this.VN_interaction_has_happened == false)
    val newState: TransitionState = new TransitionState(this)
    newState.words = new util.ArrayList[String](this.words)
    newState.lemmas = new util.ArrayList[String](this.lemmas)
    newState.pos = new util.ArrayList[String](this.pos)
    newState.deprel = Arrays.copyOf(this.deprel, deprel.length + this.words.size)
    val lastId: Int = this.words.size
    newState.heads = Arrays.copyOf(this.heads, newState.heads.length + words.size)
    var i: Int = lastId
    while (i < lastId + words.size) {
      {
        newState.deprel(i) = words.get(i - lastId)
        // TODO deprel We just need the correct length, the content will be overwritten
        newState.input.add(i)
        newState.heads(i) = -1
      }
      {
        i += 1; i - 1
      }
    }
    newState.words.addAll(words)
    newState.lemmas.addAll(lemmas)
    newState.pos.addAll(pos)
    newState.action = TransitionState.action_types.ADDWORD
    // never undo a new word
    newState.rollBackTo = newState
    return newState
  }

  /**
    * Shifts a word from Input to Stack
    * This operation does not generate a rollback point.
    */
  def shift: TransitionState = {
    if (input.size == 0) return null
    val newState: TransitionState = new TransitionState(this)
    val toShift: Int = newState.input.get(0)
    newState.input.remove(0)
    newState.stack.add(0, toShift)
    newState.action = TransitionState.action_types.SHIFT
    return newState
  }

  /**
    * Predict a Virtual node
    * TODO remove deprel again
    * This operation does not generate a rollback point.
    *
    * @return resulting state or null if limit of VNs reached or input is not empty
    */
  def addVN(pos: String): TransitionState = {
    if (num_vns >= TransitionState.MAX_NUM_VNS || input.size > 0) return null
    val newState: TransitionState = extendWords(Lists.newArrayList(TransitionState.VN_NAME), Lists.newArrayList(pos), Lists.newArrayList(TransitionState.VN_NAME))
    newState.num_vns += 1
    // if we do something with virtual nodes, mark it.
    newState.VN_interaction_has_happened = true
    newState.action = TransitionState.action_types.ADDVN
    // adding a VN should not set a new rollback point, but extendWords does so.
    // revert that.
    newState.rollBackTo = this.rollBackTo
    return newState
  }

  def reduce(`type`: TransitionState.ReduceType): TransitionState = {
    return reduce(`type`, null)
  }

  /**
    * Reduce actions according to Gomez-Rodriguez et al. (2014)
    * This operation generates a rollback point if no VN interaction has happened.
    *
    * @param type what reduce should be done
    * @return the resulting new state
    */
  def reduce(`type`: TransitionState.ReduceType, possibleEdges: THashMap[String, THashSet[String]]): TransitionState = {
    //TODO assign dependency relations
    // reduce needs two elements unless we want to attach to root
    if (stack.size < 2 - (if ((`type` eq TransitionState.ReduceType.ROOT)) 1
    else 0)) return null
    // Need three elements for TWO* since we attach the uppermost with the third
    if (((`type` eq TransitionState.ReduceType.TWOLEFT) || (`type` eq TransitionState.ReduceType.TWORIGHT)) && stack.size < 3) return null
    val newState: TransitionState = new TransitionState(this)
    var head: Int = -1
    var dep: Int = -1
    `type` match {
      case LEFT =>
        head = newState.stack.get(0)
        dep = newState.stack.remove(1)
        newState.action = TransitionState.action_types.LEFT
        break //todo: break is not supported
      case RIGHT =>
        head = newState.stack.get(1)
        dep = newState.stack.remove(0)
        newState.action = TransitionState.action_types.RIGHT
        break //todo: break is not supported
      case TWOLEFT =>
        head = newState.stack.get(0)
        dep = newState.stack.remove(2)
        newState.action = TransitionState.action_types.TWOLEFT
        break //todo: break is not supported
      case TWORIGHT =>
        head = newState.stack.get(2)
        dep = newState.stack.remove(0)
        newState.action = TransitionState.action_types.TWORIGHT
        break //todo: break is not supported
      case ROOT =>
        if (this.hasRoot) {
          // can't create a second root
          return null
        }
        newState.action = TransitionState.action_types.ROOT
        head = 0
        dep = newState.stack.get(0)
        newState.hasRoot = true
    }
    if (possibleEdges != null && !possibleEdges.get(pos.get(dep)).contains(pos.get(head))) return null
    // disallow reattachments of already attached nodes
    if (heads(dep) != -1) return null
    // disallow attachments between non-VNs if VN attachments are already present
    if (this.VN_interaction_has_happened && !(is_vn(words.get(head)) || is_vn(words.get(dep)) || head == 0)) return null
    newState.heads(dep) = head
    if (!this.VN_interaction_has_happened) {
      newState.rollBackTo = newState
    }
    return newState
  }

  /**
    * produces states by attaching the newest (non-virtual) word to any other word without creating circular attachments
    * In all those states, the lastWord is removed from stack or input to prevent later cycle creation.
    *
    * @param possibleEdges
    * @return
    */
  def attachNewWord(possibleEdges: THashMap[String, THashSet[String]]): util.Set[TransitionState] = {
    val result: util.Set[TransitionState] = new util.HashSet[TransitionState]
    var lastWord: Int = words.indexOf(TransitionState.VN_NAME)
    if (lastWord == -1) lastWord = words.size - 1
    // don't reattach word
    if (heads(lastWord) > -1) return result
    headFind //todo: labels is not supported
    var i: Int = 1
    while (i < lastWord) {
      {
        if (possibleEdges != null && !possibleEdges.get(pos.get(lastWord)).contains(pos.get(i))) continue //todo: continue is not supported
      // prevent cycles
      var curHead: Int = i
        while (curHead > 0) {
          {
            curHead = heads(curHead)
            if (curHead == lastWord) continue headFind //todo: continue is not supported
          }
        }
        val newState: TransitionState = new TransitionState(this)
        newState.heads(lastWord) = i
        // lastWord needs to be boxed to call the correct remove method
        newState.stack.remove(lastWord.asInstanceOf[Integer])
        newState.input.remove(lastWord.asInstanceOf[Integer])
        newState.VN_interaction_has_happened = true
        newState.rollBackTo = newState
        result.add(newState)
      }
      {
        i += 1; i - 1
      }
    }
    return result
  }

  /**
    * Creates all possible states from this one by only creating edges between
    * PoS licensed by the possibleEdges map (key = dep, value = head)
    */
  def buildNewStates(possibleEdges: THashMap[String, THashSet[String]]): util.Set[TransitionState] = {
    val result: util.Set[TransitionState] = new util.HashSet[TransitionState]
    var ts: TransitionState = shift
    if (ts != null) {
      result.add(ts)
    }
    for (r <- TransitionState.ReduceType.values) {
      ts = reduce(r, possibleEdges)
      if (ts != null) {
        result.add(ts)
      }
    }
    //ts = addVN("NN", "OBJA");
    ts = addVN("NVIRT")
    if (ts != null) result.add(ts)
    //ts = addVN("VVFIN", "S");
    ts = addVN("VVIRT")
    if (ts != null) result.add(ts)
    // result.addAll(attachNewWord(possibleEdges));
    return result
  }

  def is_vn(word: String): Boolean = {
    return word.contains(TransitionState.VN_NAME)
  }

  def stateSequenceString: String = {
    val prevState: TransitionState = this.getPreviousState
    val desc: StringBuilder = new StringBuilder
    if (prevState != null) {
      desc.append(action.toString).append("\n")
    }
    else {
      desc.append("initial State\n")
    }
    desc.append("-----------------------\n")
    desc.append(Arrays.toString(getHeads)).append("\n")
    desc.append(this.toString).append("\n\n")
    if (previousState == null) return desc.toString
    return previousState.stateSequenceString + desc.toString
  }

  override def toString: String = {
    val sb: StringBuilder = new StringBuilder
    sb.append("Heads: ")
    var i: Int = 1
    while (i < heads.length) {
      {
        sb.append(i)
        sb.append(":").append(words.get(i)).append("->").append(deprel(i)).append("->")
        sb.append(heads(i))
        sb.append(" ")
      }
      {
        i += 1; i - 1
      }
    }
    sb.append("\t Stack: ")
    import scala.collection.JavaConversions._
    for (i <- stack) {
      sb.append(i)
      sb.append(": ").append(words.get(i)).append(" ")
    }
    sb.append("\t Input: ")
    import scala.collection.JavaConversions._
    for (i <- input) {
      sb.append(i)
      sb.append(": ").append(words.get(i)).append(" ")
    }
    // sb.append("\tScore: ").append(score);
    return sb.toString
  }

  def getPreviousState: TransitionState = {
    return previousState
  }

  /**
    * Two states are equal iff they have the same words, stack etc. as well as a compatible rollback point.
    * Note that the rollback points don't need to be equal because their rollback points are not checked.
    */
  override def equals(o: Any): Boolean = {
    if (this eq o) return true
    if (o == null || (getClass ne o.getClass)) return false
    val that: TransitionState = o.asInstanceOf[TransitionState]
    if (!deprel == that.deprel) return false
    if (!Arrays.equals(heads, that.heads)) return false
    if (!input == that.input) return false
    if (!pos == that.pos) return false
    if (this.rollBackTo == null ^ that.rollBackTo == null) return false
    if (this.rollBackTo != null && (this.rollBackTo.uniqueIntList ne that.rollBackTo.uniqueIntList)) return false
    return stack == that.stack && words == that.words
  }

  override def hashCode: Int = {
    var result: Int = Arrays.hashCode(heads)
    result = 31 * result + stack.hashCode
    result = 31 * result + input.hashCode
    result = 31 * result + words.hashCode
    result = 31 * result + pos.hashCode
    result = 31 * result + deprel.hashCode
    if (this.rollBackTo != null && this.rollBackTo.uniqueIntList != null) result = 31 * result + this.rollBackTo.uniqueIntList.hashCode
    return result
  }

  /**
    * produces a String that contains all information for creating featureData.
    * Allows us to share the same featureData across different TransitionStates.
    *
    * @return UID for the fd
    */
  private val _fd_string: String = null

  def fdInformation: String = {
    if (_fd_string != null) return _fd_string
    val res: StringBuilder = new StringBuilder
    import scala.collection.JavaConversions._
    for (i <- words) res.append(i).append("_:_")
    import scala.collection.JavaConversions._
    for (i <- lemmas) res.append(i).append("_:_")
    import scala.collection.JavaConversions._
    for (i <- pos) res.append(i).append("_:_")
    return res.toString
  }

  private var _uniqueIntList: TIntArrayList = null

  def uniqueIntList: TIntArrayList = {
    // Don't cache states with VNs for now, it is too much work to get it right
    if (VN_interaction_has_happened) return null
    if (_uniqueIntList != null) return _uniqueIntList
    _uniqueIntList = new TIntArrayList(input.size + stack.size + heads.length + 2)
    _uniqueIntList.addAll(input)
    _uniqueIntList.add(-9876)
    _uniqueIntList.addAll(stack)
    _uniqueIntList.add(-9876)
    _uniqueIntList.add(heads.length)
    return _uniqueIntList
  }
}

*/
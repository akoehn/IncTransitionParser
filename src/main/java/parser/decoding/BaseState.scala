package parser.decoding

import parser.DependencyInstance
import parser.DependencyParser

/**
  * Created by koehn on 4/29/15.
  */
trait BaseState[T <: BaseState[T]] {
  def getPreviousState: T

  def lastWord: Int

  def getWords: Array[String]

  def getHeads: Array[Int]

  def getDeprel: Array[String]

  def toInstanceUninstantiated: DependencyInstance

  def toInstance(parser: DependencyParser): DependencyInstance

  def getInstance: DependencyInstance

  /**
    * produces a String that contains all information for creating featureData.
    * Allows us to share the same featureData across different states.
    *
    * @return UID for the fd
    */
  def fdInformation: String

  private var score: Double = 0.0

  private var rbgScore: Double = 0.0

  def setrbgScore(newScore: Double) = rbgScore = newScore

  def getrbgScore: Double = rbgScore

  // def setScore(newScore: Double): Unit = score = newScore

  def getScore: Double = score

  def setScore(newScore: Double): Unit = score = newScore

  def addToScore(newScore: Double): Unit = score += newScore

  def stateSequenceString(): String
}

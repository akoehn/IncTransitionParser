package parser.decoding;

import com.github.benmanes.caffeine.cache.LoadingCache;
import com.google.common.base.Stopwatch;
import gnu.trove.map.hash.THashMap;
import gnu.trove.set.hash.THashSet;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import parser.*;
import parser.scoring.LstmScorer;
import utils.FeatureVector;
import utils.Utils;

import java.util.stream.IntStream;

public class IncHillClimbingDecoder extends HillClimbingDecoder {
    private static final Logger logger = LoggerFactory.getLogger(IncHillClimbingDecoder.class);
    private final LstmScorer lstmScorer;
    private LoadingCache<LocalFeatureCacheKey, FeatureVector> fvCache;
    private LoadingCache<LocalFeatureCacheKey, Double> scoreCache;
	private ParserConf pc;
    public IncHillClimbingDecoder(ParserConf pc, Options options, LstmScorer lstmScorer,
                                  LoadingCache<LocalFeatureCacheKey, FeatureVector> fvCache,
                                  LoadingCache<LocalFeatureCacheKey, Double> scoreCache) {
        super(options);
		this.pc = pc;
        this.lstmScorer = lstmScorer;
		this.lstmScorer.setTraining(false);
		this.fvCache = fvCache;
		this.scoreCache = scoreCache;
    }

    public void setPossibleEdgesBetweenPOS(THashMap<String, THashSet<String>> possibleEdgesBetweenPOS) {
        for (HillClimbingTask t: tasks) {
            t.sampler.setPossibleEdgesBetweenPOS(possibleEdgesBetweenPOS);
        }
    }


    @Override
    protected HillClimbingTask createTask() {
        return new IncHillClimbingTask();
    }

    public DependencyInstance decode(DependencyInstance inst,
                                     DependencyParser depParser) {
        return decode(inst, depParser, (Integer)pc.maxNumVirtualNodes().apply());
    }

    public DependencyInstance decode(DependencyInstance inst,
                                     DependencyParser depParser,
                                     int maxNumPredNodes) {

        Stopwatch sw = Stopwatch.createStarted();
        DependencyInstance bestInstance = null;
        double bestScore =  Double.NEGATIVE_INFINITY;
        for (int numPredNodes = 0; numPredNodes < maxNumPredNodes + 1; numPredNodes++) {
            lstmScorer.newPrefix(inst, inst.lastNormalWord);
            lfd = new IncLocalFeatureData(inst, depParser, false, fvCache, scoreCache, inst.lastNormalWord);
            //lfd = new LocalFeatureData(inst, depParser, false);
            gfd = ((IncLocalFeatureData) lfd).gfd();
            // gfd = new GlobalFeatureData(lfd);
            DependencyInstance newInst = super.decode(inst, lfd, gfd, addLoss);
            double newScore = getBestScore();
            logger.trace(String.format("score with %d prediction nodes: %f", numPredNodes, newScore));
            if (newScore > bestScore) {
                bestScore = newScore;
                bestInstance = newInst;
            }
            inst = inst.addPredictionNode(depParser.pipe.dictionaries);
        }
        logger.trace(sw.toString());
        return bestInstance;
    }

    public class IncHillClimbingTask extends HillClimbingDecoder.HillClimbingTask {
        @Override
        public void run() {

            n = inst.length;
            converge = addLoss ? options.numTrainConverge : options.numTestConverge;

            if (dfslis == null || dfslis.length < n) {
                dfslis = new int[n];
            }
            if (arcLis == null)
                arcLis = new DependencyArcList(n, options.useHO);
            else
                arcLis.resize(n, options.useHO);

            if (inst.lastNormalWord == inst.length -1)
                // check for all words whether they can currently be only attached to prediction nodes
                // If we find such a word, abort and only work on sturctures with prediction
                for (int i= 1; i<= inst.lastNormalWord; i++) {
                    String pos = inst.postags[i];
                    THashSet<String> pHeads = sampler.possibleEdgesBetweenPOS.get(pos);
                    boolean foundPossibleHead = false;
                    // We are only interested in words that could be attached to a prediction node
                    // but there is currently none
                    if (pHeads == null) {
                        // foundPossibleHead = true;
                        break;
                    }
                    if (! pHeads.contains("VVIRT"))
                        continue;
                    for (int j= 0; j <= inst.lastNormalWord; j++) {
                        if (pHeads.contains(inst.postags[j])) {
                            foundPossibleHead = true;
                            break;
                        }
                    }
                    if (!foundPossibleHead) {
                        logger.error("No attachment in prefix possible");
                        bestScore = Double.NEGATIVE_INFINITY;
                        return;
                    }
                }

            while (!stopped) {

                DependencyInstance now = null;
                for (int i = 0; i<10 && now == null; i++) {
                    now = sampler.uniformRandomWalkSampling(
                            inst, lfd, addLoss);
                    // if now == null, we are lacking a prediction node, try again and hope for another run
                    // see uniformRandomWalkSampling comments for more explanation
                }
                // tried 10 times, didn't work.  Give up.
                if (now == null)
                    return;

                //if (now == null) {
                //    bestScore = Double.NEGATIVE_INFINITY;
                //    continue;
                //}

                // hill climb
                int[] heads = now.heads;
                //int[] deplbids = now.deplbids;

                arcLis.constructDepTreeArcList(heads);
                if (arcLis.left != null && arcLis.right != null)
                    arcLis.constructSpan();
                if (arcLis.nonproj != null)
                    arcLis.constructNonproj(heads);

                // int cnt = 0;
                boolean more = true;
                while (more) {
                    more = false;
                    depthFirstSearch(heads);
                    Utils.Assert(size == n-1);
                    for (int i = 0; i < size; ++i) {
                        int m = dfslis[i];

                        int bestHead = heads[m];
                        double maxScore = calcScore(heads, m, arcLis);

                        int lastHead = bestHead;
                        THashSet<String> pHeads = sampler.possibleEdgesBetweenPOS.get(inst.postags[m]);
                        int[] possibleHeads = {};
                        if (pHeads != null) {
                            int finalM = m;
                            possibleHeads = IntStream.range(1, inst.length).filter((int x) -> x != finalM).filter((int x) -> pHeads.contains(inst.postags[x])).toArray();
                        }
                        //if (possibleHeads.length == 0) {
                         if (pHeads == null) {
                            possibleHeads = IntStream.range(1, inst.length).toArray();
                        }
                        for (int h: possibleHeads) {
                            if (/*h != m &&*/ h != bestHead /*&& !lfd.isPruned(h, m)*/
                                    && !isAncestorOf(heads, m, h)) {
                                heads[m] = h;
                                arcLis.update(m, lastHead, h, heads);
                                //checkUpdateCorrect(heads, arcLis);
                                lastHead = h;
                                double score = calcScore(heads, m, arcLis);
                                if (score > maxScore) {
                                    more = true;
                                    bestHead = h;
                                    maxScore = score;
                                }
                            }
                        }
                        heads[m] = bestHead;
                        arcLis.update(m, lastHead, bestHead, heads);
                        //checkUpdateCorrect(heads, arcLis);
                    }
                }


                double score = calcScore(now);
                synchronized (pred) {
                    ++totRuns;
                    if (score > bestScore) {
                        bestScore = score;
                        unchangedRuns = 0;
                        pred.heads = heads;
                        // pred.deplbids = deplbids;
                    } else {
                        ++unchangedRuns;
                        if (unchangedRuns >= converge)
                            stopped = true;
                    }

                }
            }
        }

        @Override
        protected double calcScore(int[] heads, int m, DependencyArcList arcLis) {
            double rbgscore = lfd.getScore(heads, arcLis) + gfd.getScore(heads, arcLis);
            // double rbgscore = super.calcScore(heads, m, arcLis);
            if (lstmScorer.lstmDisabled())
                return rbgscore;
            synchronized (lstmScorer) {
                return lstmScorer.foScoreFromHeadsAndScore(heads, (float) (rbgscore));
            }
        }

        @Override
        protected double calcScore(DependencyInstance now) {
            DependencyArcList arcLis = new DependencyArcList(now.heads, options.useHO);
            return calcScore(now.heads, 0,arcLis);
        }
    }
}

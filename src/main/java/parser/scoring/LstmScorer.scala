package parser.scoring

import java.util.concurrent.BlockingQueue

import com.github.benmanes.caffeine.cache.{Cache, Caffeine}
import com.typesafe.scalalogging.Logger
import edu.cmu.dynet._
import org.rogach.scallop.Subcommand
import org.slf4j.LoggerFactory
import parser.decoding.BaseState
import parser.{DependencyInstance, DependencyParser, DependencyPipe, ParserConf}
import utils.DictionarySet.DictionaryTypes

import scala.collection.mutable.ArrayBuffer


class LstmConf(name: String) extends Subcommand(name) {
  val wembDim = opt[Int](default = Some(200), validate = 0 < _, descr = "dimension of word embeddings")
  val pembDim = opt[Int](default = Some(25), validate = 0 < _, descr = "dimension of PoS embeddings")
  val lstmDim = opt[Int](default = Some(125), validate = 0 < _, descr = "dimension of lstm layer")
  val lstmLayers = opt[Int](default = Some(2), validate = 0 < _, descr = "number of LSTM layers")
  val hiddenDim = opt[Int](default = Some(100), validate = 0 < _, descr = "dimension of hidden layer")
  val useDropOut = toggle(default = Some(true), descrYes = "use dropout for lstm training")
  val usePos = toggle(default = Some(true), descrYes = "use Pos as an input in the LSTMs")
  val mostViolating = opt[Boolean](default = Some(true), descr = "update against most violating tree. " +
                                                                 "alternatively, update against best scoring")
  val useFirstOrderScorer = toggle(default = Some(true), descrYes = "use first order scorer")
  val useTreeScorer = toggle(default = Some(false), descrYes = "use treeLSTM scorer")
}

object LstmScoringRunner{
  val logger = Logger(LoggerFactory.getLogger(this.getClass))
}

class LstmScoringRunner[T<: BaseState[T]] (scorer: LstmScorer,
                                           in: BlockingQueue[Option[T]],
                                           out: BlockingQueue[Option[T]],
                                           numNoneToConsume: Int,
                                           parser: DependencyParser) extends AbstractScorer[T](in,out,numNoneToConsume) {
  override def getLogger = LstmScoringRunner.logger
  val trees = new ArrayBuffer[(T, Expression)]()
  override def processState(state: T) = {
    val expression = scorer.expressionForInstance(state.toInstance(parser), state.getScore.toFloat)
    trees.+=((state, expression))
    if (trees.length > 10) {
      cleanUp()
    }
  }

  override def cleanUp(): Unit = {
    // compute sum for several trees.  We are not interested in the sum but in the speed up
    // due to autobatching
    if (trees.nonEmpty){
      Expression.sum(trees.map(_._2))
      for (elem <- trees) {
        val expr = elem._2
        val state = elem._1
        try {
          state.setScore(ComputationGraph.incrementalForward(expr).toFloat())
        } catch {
          case x : Throwable => logger.error(x.toString)
        }
        logger.debug("computed score")
        out.put(Some(state))
      }
      trees.clear()
    }
  }
}


class LstmScorer
(
  pipe: DependencyPipe,
  pconf: ParserConf
) {

  val dicts = pipe.dictionaries
  val conf = pconf.lstmSubcommand
  val model: ParameterCollection = new ParameterCollection()
  val lstmDisabled = !conf.useTreeScorer() && !conf.useFirstOrderScorer()

  //Initialize.initialize(Map("autobatch" -> 1))

  Initialize.initialize()

  private val freqCounter = pipe.frequencyCounter

  // use dimensions of pre-trained embeddings if they are supplied.
  private val wEmbDim =
    if (pipe.wordVectors != null && pipe.wordVectors.length > 0) {
      var i = 0
      while (pipe.wordVectors(i) == null) i += 1
      pipe.wordVectors(i).length
    } else {
      conf.wembDim()
    }
  private val pEmbDim = if (pconf.filterPos()) 0 else conf.pembDim()
  private val lstmDim = conf.lstmDim()
  private val hiddenDim = conf.hiddenDim()
  private val layers = conf.lstmLayers()

  var isTraining: Boolean = false
  def setTraining(b: Boolean): Unit = {isTraining = b}


  /*
  TODO:
   - cpos tags?

   Later:
   - morph & pos prediction for multitask learning?
   */

  private val forwardLSTM = new CoupledLstmBuilder(layers, wEmbDim + pEmbDim, lstmDim, model)
  private val backwardLSTM = new CoupledLstmBuilder(layers, wEmbDim + pEmbDim, lstmDim, model)
  private val predNodeEmb = model.addParameters(Dim(2*lstmDim))
  private var predNodeExpr: Expression = null

  private val wEmbeddings = model.addLookupParameters(dicts.size(DictionaryTypes.WORD), Dim(wEmbDim))
  private val unkEmbedding = model.addParameters(Dim(wEmbDim))
  private val pEmbeddings = if (pEmbDim > 0)
    model.addLookupParameters(dicts.size(DictionaryTypes.POS), Dim(pEmbDim))
  else
    null
  // MLP weights split between head and dep part (see comment below) FOH == First order head, FOM == FO modifier
  private val hiddenFOH = model.addParameters(Dim(hiddenDim, lstmDim*2))
  private val hiddenFOM = model.addParameters(Dim(hiddenDim, lstmDim*2))
  private val hiddenBias = model.addParameters(Dim(hiddenDim))
  private val outWeight = model.addParameters(Dim(hiddenDim))

  val theta = model.addParameters(Dim(1))

  private val treeLSTMBuilder = new BidirectionalTreeLSTMBuilder(1, lstmDim*2, lstmDim*2, model)
  private val treeOutWeight = model.addParameters(Dim(lstmDim*2))

  private var hiddenFOHExpr: Expression = null
  private var hiddenFOMExpr: Expression = null
  private var hiddenBiasExpr: Expression = null
  private var outWeightExpr: Expression = null

  private val logger = Logger(LoggerFactory.getLogger("LstmScorer"))

  private val edgeCache: Cache[(Int, Int), Expression] = Caffeine.newBuilder().build[(Int, Int), Expression]
  private val tokenCache: Cache[(Int, Boolean), Expression] = Caffeine.newBuilder().build[(Int, Boolean), Expression]
  private var currLastWord = 0

  private val trainer = if (false) new SimpleSGDTrainer(model, 0.5f) else new AmsgradTrainer(model)

  def loadModel(modelFile: String): Unit = {
    if (lstmDisabled)
      return
    new ModelLoader(modelFile + ".lstm").populateModel(model)
  }

  def saveModel(outModelFile: String): Unit = {
    val ms = new ModelSaver(outModelFile + ".lstm")
    ms.addModel(model)
    ms.done()
  }

  private val worddict = pipe.dictionaries.get(DictionaryTypes.WORD)
  private val wembdict = pipe.dictionaries.get(DictionaryTypes.WORDVEC)

  private val wordembeddings = new ArrayBuffer[FloatVector]()
  if (pipe.wordVectors != null) {
    for (elem <- pipe.wordVectors) {
      if (elem != null) {
        val floatVec = elem.toSeq.map(_.toFloat)
        wordembeddings.append(FloatVector.Seq2FloatVector(floatVec))
      } else {
        wordembeddings.append(null)
      }
    }
  }

  private def getWordVector(word: String, enableDropout: Boolean): Expression = {
    val wvid: Int = wembdict.lookupIndex(word, false)
    // maybe we have seen the word in training (so there is an id) but have no embedding for it
    val wemb = if (wvid <= 0) null else wordembeddings(wvid)
    // handle unknown words and dropout
    if (wemb == null
      || (enableDropout && freqCounter.shouldDropOut(worddict.lookupIndex(word, false)))) {
      return Expression.parameter(unkEmbedding)
    } else {
      return Expression.input(Dim(wEmbDim), wemb)
    }
  }

  val useDropOut = conf.useDropOut()
  /**
    * initializes an LSTM and with input from given instance with sequence determined by seq.
    * @return Expressions generated by the lstm
    */
  private def initLSTMWithSequence(lstm: RnnBuilder, seq: Seq[Int], inst: DependencyInstance): Seq[Expression] = {
    lstm.startNewSequence()
    seq map ((i:Int) => {
      // for some reason, someone decided to have the ids 1-based, so substract one from formid and posid
      val formid = inst.formids(i)
      val wExp = getWordVector(inst.forms(i), isTraining && useDropOut)
      if (pEmbDim > 0) {
        val posid = inst.postagids(i)
        val pExp = Expression.lookup(pEmbeddings, posid-1)
        lstm.addInput(Expression.concatenate(wExp, pExp))
      } else {
        lstm.addInput(wExp)
      }
    })
  }

  // the lstm output layer for the current sequence, not including prediction nodes
  private var lstmOutExpressions: Seq[Expression] = null

  def newPrefix(inst: DependencyInstance, lastWord: Int): Unit = {
    if (lstmDisabled)
      return
    currLastWord = lastWord
    ComputationGraph.clear()
    forwardLSTM.newGraph()
    backwardLSTM.newGraph()
    if (conf.useTreeScorer())
      treeLSTMBuilder.newGraph()

    val forwardRes = initLSTMWithSequence(forwardLSTM, 0 to lastWord, inst)
    val backwardRes = initLSTMWithSequence(forwardLSTM, (0 to lastWord).reverse, inst).reverse

    // create single output sequence from both lstms
    lstmOutExpressions = (forwardRes, backwardRes).zipped.map((fwd,bwd) => Expression.concatenate(fwd, bwd))
    predNodeExpr = Expression.parameter(predNodeEmb)

    hiddenFOHExpr = Expression.parameter(hiddenFOH)
    hiddenFOMExpr = Expression.parameter(hiddenFOM)
    hiddenBiasExpr = Expression.parameter(hiddenBias)
    outWeightExpr = Expression.transpose(Expression.parameter(outWeight))

    invalidateCaches()
  }

  private def invalidateCaches(): Unit = {
    edgeCache.invalidateAll()
    tokenCache.invalidateAll()
  }

  /**
    * Creates a Bidirectional TreeLSTM for the given instance
    */
  private def createTreeLSTM(inst: DependencyInstance): Expression = {
    val childList = new ArrayBuffer[ArrayBuffer[Int]]
    var i = 0
    while (i < inst.heads.length) {
      childList.append(new ArrayBuffer[Int]())
      i += 1
    }
    i = 1
    while (i < inst.heads.length) {
      childList(inst.heads(i)).append(i)
      i += 1
    }
    treeLSTMBuilder.startNewSequence()
    treeLSTMBuilder.setNumElements(inst.heads.length)
    Expression.dotProduct(treeLSTMFromChildList(childList, childList(0)(0)),
                          Expression.parameter(treeOutWeight))
  }

  private def treeLSTMFromChildList(childList: Seq[Seq[Int]], idx: Int): Expression = {
    val currChildren = childList(idx)
    currChildren.foreach(treeLSTMFromChildList(childList, _))
    treeLSTMBuilder.addInput(idx, childList(idx), elementExpression(idx))
  }

  /* the lstm only spans the words, prediction nodes are their own
   * parameters.  Choose the correct one.
   */
  @inline
  private def elementExpression(idx: Int): Expression = {
    if (idx > currLastWord)
      predNodeExpr
    else
      lstmOutExpressions(idx)
  }

  /*
   * We use the optimization by Kiperwasser & Goldberg (2016):
   * Instead of doing tanh(W*concat(dep, head) + b)
   * we compute the multiplication with W for dep and head separately.
   * this allows us to re-use the computations for different dep and head combinations.
   * W*concat(dep, head) = dep * W_d + head * W_h
   */
  private def createEdgeExpression(dep: Int, head: Int): Expression = {
    val headExpr = expressionForToken(head, true)
    val depExpr = expressionForToken(dep, false)
    val innerSum = headExpr + depExpr + hiddenBiasExpr
    outWeightExpr * Expression.tanh(innerSum)
  }

  /*
   * cache expressions for tokens
   */
  @inline
  private def expressionForToken(idx: Int, isHead: Boolean): Expression =  {
    tokenCache.get((idx, isHead),
      (depHead) => {
        val weightVector = if (depHead._2) hiddenFOHExpr else hiddenFOMExpr
        weightVector * elementExpression(depHead._1)
      })
  }

  def foScoreFromHeadsAndScore(heads: Array[Int], score: Float): Float = {
    val scores = new ArrayBuffer[Expression]()
    foExpressionsFromHeads(heads, scores)
    val exp = Expression.sum(scores:_*) + (Expression.input(score) * Expression.parameter(theta))
    ComputationGraph.incrementalForward(exp).toFloat()
  }

  def foExpressionsFromHeads(heads: Array[Int], scores: ArrayBuffer[Expression]): Unit = {
    scores.appendAll(1 until heads.length map ((dep) => {
      edgeCache.get((dep, heads(dep)),
        (depHead: (Int, Int)) => createEdgeExpression(depHead._1, depHead._2))
    }))
  }

  def expressionForInstance(inst: DependencyInstance, score: Float): Expression = {
    val scores = new ArrayBuffer[Expression]()
    if (conf.useFirstOrderScorer()) {
      scores.appendAll(1 until inst.length map ((dep) => {
        edgeCache.get((dep, inst.heads(dep)),
          (depHead: (Int, Int)) => createEdgeExpression(depHead._1, depHead._2))
      }))
    }
    if (conf.useTreeScorer())
      scores.append(createTreeLSTM(inst))
    Expression.sum(scores:_*) +  Expression.input(score) * Expression.parameter(theta)
  }

  /**
    * performs a training update against mostViolating or bestScore instance, dependending on
    * conf.mostViolating.  Does not perform an update if lstm scoring is disabled or
    * the instance to update against has a lower score than leastErrorsScore.
    * @param mostViolating
    * @param mostViolatingScore
    * @param bestScore
    * @param bestScoreScore
    * @param leastErrors
    * @param leastErrorsScore
    * @param errorDiff
    */
  def train(mostViolating: DependencyInstance,
            mostViolatingScore: Float,
            bestScore: DependencyInstance,
            bestScoreScore: Float,
            leastErrors: DependencyInstance,
            leastErrorsScore: Float,
            errorDiff: Float): Unit = {
    // only perform update if one of the scorers is active
    if (! (conf.useTreeScorer() || conf.useFirstOrderScorer()))
      return
    val predicted = if (conf.mostViolating()) mostViolating else bestScore
    val predictedScore = if (conf.mostViolating()) mostViolatingScore else bestScoreScore
    if (leastErrorsScore >= predictedScore + errorDiff)
      // only perform an update if the best is worse
      return
    val predExp = expressionForInstance(predicted, predictedScore)
    val leastErrExp = expressionForInstance(leastErrors, leastErrorsScore)
    val lossExpr = predExp - leastErrExp + errorDiff
    val loss = ComputationGraph.incrementalForward(lossExpr).toFloat()
    ComputationGraph.backward(lossExpr)
    // ComputationGraph.printGraphViz()
    trainer.update()
    ComputationGraph.invalidate()
  }

}

package parser.scoring

import java.util
import java.util.concurrent.{BlockingQueue, TimeUnit}

import com.typesafe.scalalogging.Logger
import org.slf4j.LoggerFactory
import parser.decoding.BaseState

abstract class AbstractScorer[T<: BaseState[T]] (in: BlockingQueue[Option[T]],
                                                 out: BlockingQueue[Option[T]],
                                                 numNoneToConsume: Int) extends Runnable {
  def getLogger(): Logger
  val logger = getLogger()
  override def run() = {
    var numNones = 0
    while (numNones < numNoneToConsume) {
      in.take() match {
        case Some(x) =>
          logger.trace(s"scoring $x")
          processState(x)
        case None => {
          numNones += 1
          logger.trace(s"received None number $numNones of $numNoneToConsume")
        }
      }
    }
    logger.trace("finished, cleaning up")
    cleanUp()
    // put all Nones back which we consumed before
    logger.trace(s"putting ${numNoneToConsume} nones")
    for (i <- 0 until numNoneToConsume)
      out.put(None)
  }

  def processState(state: T)
  def cleanUp()
}

package parser.scoring

import java.util
import java.util.concurrent.BlockingQueue

import com.typesafe.scalalogging.Logger
import org.slf4j.LoggerFactory
import parser.AbstractTransitionParser
import parser.decoding.BaseState

object RBGScorer{
  val logger = Logger(LoggerFactory.getLogger(this.getClass))
}

class RBGScorer[T<: BaseState[T]] (scorer: AbstractTransitionParser[T],
                                   in: BlockingQueue[Option[T]],
                                   out: BlockingQueue[Option[T]]) extends AbstractScorer[T](in, out, 1) {
  override def getLogger(): Logger = RBGScorer.logger
  override def processState(state: T): Unit = {
    val score = scorer.computeScore(state)
    state.setrbgScore(score)
    state.addToScore(score)
    out.put(Some(state))
  }

  override def cleanUp(): Unit = {}

}

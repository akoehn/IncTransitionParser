package parser;

import static utils.DictionarySet.DictionaryTypes.DEPLABEL;
import static utils.DictionarySet.DictionaryTypes.POS;
import static utils.DictionarySet.DictionaryTypes.WORD;
import static utils.DictionarySet.DictionaryTypes.WORDVEC;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.io.Serializable;
import java.util.*;
import java.io.*;

import org.apache.commons.lang3.ArrayUtils;
import parser.Options.PossibleLang;
import parser.DependencyParser;

import utils.Alphabet;
import utils.Dictionary;
import utils.DictionarySet;

public class DependencyInstance implements Serializable {
	
	public enum SpecialPos {
		C, P, PNX, V, AJ, N, OTHER,
	}
	
	private static final long serialVersionUID = 1L;
	
	public int length;
	public int lastNormalWord;
	
	// FORM: the forms - usually words, like "thought"
	public String[] forms;
	
	// LEMMA: the lemmas, or stems, e.g. "think"
	public String[] lemmas;
	
	// COARSE-POS: the coarse part-of-speech tags, e.g."V"
	public String[] cpostags;
	
	// FINE-POS: the fine-grained part-of-speech tags, e.g."VBD"
	public String[] postags;
	
	// MOST-COARSE-POS: the coarsest part-of-speech tags (about 11 in total)
	public SpecialPos[] specialPos;
	
	// FEATURES: some features associated with the elements separated by "|", e.g. "PAST|3P"
	public String[][] feats;

	// HEAD: the IDs of the heads for each element
	public int[] heads;

	// DEPREL: the dependency relations, e.g. "SUBJ"
	public String[] deprels;
	
	public int[] formids;
	public int[] lemmaids;
	public int[] postagids;
	public int[] cpostagids;
	public int[][] featids;
	public int[] wordVecIds;

	public int[] deplbids;

    public DependencyInstance() {}

    // public DependencyInstance(int length) { this.length = length; numConcurrent = new AtomicInteger(0);}


    // TODO Why were those accessible? doesn't make sense
    // TODO Why were those accessible? doesn't make sense
//    private DependencyInstance(String[] forms) {
//    	length = forms.length;
//    	this.forms = forms;
//    	this.feats = new String[length][];
//    	this.deprels = new String[length];
//    }

    public DependencyInstance(String[] forms, String[] lemmas, String[] cpostags, String[] postags,
            String[][] feats, int[] heads, String[] deprels) {
		this.length = forms.length;
		this.forms = forms;
		this.heads = heads;
		this.postags = postags;
		this.deprels = deprels;
		this.lemmas = lemmas;
    	this.feats = feats;
    	this.cpostags = cpostags;
		this.lastNormalWord = deduceLastNormalWord();
    }

    public DependencyInstance(String[] forms, String[] lemmas, String[] cpostags, String[] postags,
							  String[][] feats, int[] heads, String[] deprels, int lastNormalWord) {
		this(forms, lemmas, cpostags, postags, feats, heads, deprels);
		this.lastNormalWord = lastNormalWord;
	}
    
    public DependencyInstance(DependencyInstance a) {
    	//this(a.forms, a.lemmas, a.cpostags, a.postags, a.feats, a.heads, a.deprels);
    	specialPos = a.specialPos;
    	length = a.length;
    	heads = a.heads;
    	feats = a.feats;
    	featids = a.featids;
    	formids = a.formids;
    	forms = a.forms;
    	lemmaids = a.lemmaids;
    	lemmas = a.lemmas;
    	postags = a.postags;
    	postagids = a.postagids;
    	cpostags = a.cpostags;
    	cpostagids = a.cpostagids;
    	deprels = a.deprels;
    	deplbids = a.deplbids;
    	wordVecIds = a.wordVecIds;
		lastNormalWord = a.lastNormalWord;
    }

    private int deduceLastNormalWord() {
		for (int i = 1; i < forms.length; i++) {
			if (forms[i].equals(AbstractTransitionParser.VN_NAME())) {
				return this.lastNormalWord = i - 1;
			}
		}
		return forms.length - 1;
	}

    public DependencyInstance shorten(int newLength) {
        DependencyInstance newInst = new DependencyInstance(this);
        newInst.lastNormalWord = newLength;

        // if the lenght of the prefix is n words, we need to keep n+1 elements
        // because of the addition root node at 0
        newLength += 1;
		newInst.length = newLength;
        newInst.specialPos = ArrayUtils.subarray(specialPos, 0, newLength);
        newInst.heads = ArrayUtils.subarray(heads, 0, newLength);
        for (int i = 1; i < newLength; i++) {
        	newInst.heads[i] = i-1;
		}
		newInst.featids = ArrayUtils.subarray(featids, 0, newLength);
		newInst.feats = ArrayUtils.subarray(feats, 0, newLength);
        newInst.formids = ArrayUtils.subarray(formids, 0, newLength);
        newInst.forms = ArrayUtils.subarray(forms, 0, newLength);
		newInst.lemmas = ArrayUtils.subarray(lemmas, 0, newLength);
        newInst.lemmaids = ArrayUtils.subarray(lemmaids, 0, newLength);
        newInst.postagids = ArrayUtils.subarray(postagids, 0, newLength);
		newInst.postags = ArrayUtils.subarray(postags, 0, newLength);
        newInst.cpostagids = ArrayUtils.subarray(cpostagids, 0, newLength);
		newInst.cpostags = ArrayUtils.subarray(cpostags, 0, newLength);
        newInst.deprels = ArrayUtils.subarray(deprels, 0, newLength);
        newInst.deplbids = ArrayUtils.subarray(deplbids, 0, newLength);
        newInst.featids = ArrayUtils.subarray(featids, 0, newLength);
        newInst.wordVecIds = ArrayUtils.subarray(wordVecIds, 0, newLength);

        return newInst;
    }

    /**
     * adds a prediction node to the instance.  Used for the incremental hill climbing decoder, therefore the
     * fields not needed are set to default values (head, dep label etc.)
     * @param dict DictionarySet to look up indexes
     * @return
     */
    public DependencyInstance addPredictionNode(DictionarySet dict) {
    	String pnodeName = AbstractTransitionParser.VN_NAME();
    	DependencyInstance newInst = new DependencyInstance(this);
        newInst.specialPos = ArrayUtils.add(specialPos, SpecialPos.OTHER);
        newInst.length = length + 1;
        newInst.heads =  ArrayUtils.add(this.heads, length-1);
        int fid = dict.lookupIndex(DictionarySet.DictionaryTypes.WORD, "form="+pnodeName);
        newInst.formids = ArrayUtils.add(formids, fid);
        newInst.forms = ArrayUtils.add(forms, pnodeName);
		newInst.lemmas = ArrayUtils.add(lemmas, pnodeName);
        int lid = dict.lookupIndex(DictionarySet.DictionaryTypes.WORD, "lemma="+pnodeName);
        newInst.lemmaids = ArrayUtils.add(lemmaids, lid);
        int pid = dict.lookupIndex(DictionarySet.DictionaryTypes.POS, "pos=VVIRT");
        newInst.postagids = ArrayUtils.add(postagids, pid);
		newInst.postags = ArrayUtils.add(postags, "VVIRT");
        int cpid = dict.lookupIndex(DictionarySet.DictionaryTypes.POS, "cpos=VVIRT");
        newInst.cpostagids = ArrayUtils.add(cpostagids, cpid);
		newInst.cpostags = ArrayUtils.add(cpostags, "VVIRT");
        // these are not used and set to default values
        newInst.deprels = ArrayUtils.add(this.deprels, "");
        newInst.deplbids = ArrayUtils.add(deplbids, 0);
        int[] farr = {};
        newInst.featids = ArrayUtils.add(featids, farr);
        newInst.wordVecIds = ArrayUtils.add(wordVecIds, 0);
        newInst.lastNormalWord = lastNormalWord;

        return newInst;
	}
	/**
	 * Performs a faster setInstIds by copying from another DependencyInstance up to lastNormalWord
	 * instead of performing the lookups
	 */
	public void setInstIds(DependencyParser parser, DependencyInstance prevInst, int lastNormalWord) {
		clearIdArrays();
		assert(prevInst != null);
		assert(prevInst.formids != null);
		assert(formids != null);
		// adjust for 0-basedness of the arrays
		lastNormalWord += 1;
		System.arraycopy(prevInst.formids, 0, formids, 0, lastNormalWord);
		System.arraycopy(prevInst.deplbids, 0, deplbids, 0, lastNormalWord);
		System.arraycopy(prevInst.postagids, 0, postagids, 0, lastNormalWord);
		System.arraycopy(prevInst.cpostagids, 0, cpostagids, 0, lastNormalWord);
		if (lemmas != null) {
    		System.arraycopy(prevInst.lemmaids, 0, lemmaids, 0, lastNormalWord);
		}
		// only needed if WordVecs used, but we don't know in this case.
		// if (dicts.size(WORDVEC) > 0)
		System.arraycopy(prevInst.wordVecIds, 0, wordVecIds, 0, lastNormalWord);
		System.arraycopy(prevInst.specialPos, 0, specialPos, 0, lastNormalWord);
		
		setInstIds(parser.pipe.dictionaries, parser.pipe.coarseMap, parser.pipe.conjWord, parser.pipe.options.lang, lastNormalWord);
	}
	public void setInstIds(DependencyParser parser) {
		setInstIds(parser.pipe.dictionaries, parser.pipe.coarseMap, parser.pipe.conjWord, parser.pipe.options.lang);
	}
	public void setInstIds(DictionarySet dicts, 
    		HashMap<String, String> coarseMap, HashSet<String> conjWord, PossibleLang lang) {
		clearIdArrays();
		setInstIds(dicts, coarseMap, conjWord, lang, 0);
	}

	protected void clearIdArrays() {
		formids = new int[length];
		deplbids = new int[length];
		postagids = new int[length]; 
		cpostagids = new int[length];
		featids = new int[length][];
		if (lemmas != null) {
    		lemmaids = new int[length];
		}
		// only needed if WordVecs used, but we don't know in this case.
		// if (dicts.size(WORDVEC) > 0)
		wordVecIds = new int[length];
		specialPos = new SpecialPos[length];
	}
    protected void setInstIds(DictionarySet dicts, 
						   HashMap<String, String> coarseMap, HashSet<String> conjWord, PossibleLang lang, int lastNormalWord) {
    	    			
    	for (int i = lastNormalWord; i < length; ++i) {
    		//formids[i] = dicts.lookupIndex(WORD, "form="+normalize(forms[i]));
			formids[i] = dicts.lookupIndex(WORD, "form="+forms[i]);
			postagids[i] = dicts.lookupIndex(POS, "pos="+postags[i]);
			cpostagids[i] = dicts.lookupIndex(POS, "cpos="+cpostags[i]);
			deplbids[i] = dicts.lookupIndex(DEPLABEL, deprels[i]) - 1;	// zero-based
    	}
    	
    	if (lemmas != null) {
    		for (int i = lastNormalWord ; i < length; ++i)
				lemmaids[i] = dicts.lookupIndex(WORD, "lemma="+lemmas[i]);
    			//lemmaids[i] = dicts.lookupIndex(WORD, "lemma="+normalize(lemmas[i]));
    	}

		for (int i = 0; i < length; ++i) if (feats[i] != null) {
			featids[i] = new int[feats[i].length];
			for (int j = 0; j < feats[i].length; ++j)
				featids[i][j] = dicts.lookupIndex(POS, "feat="+feats[i][j]);
		}
		
		if (dicts.size(WORDVEC) > 0) {
			for (int i = lastNormalWord; i < length; ++i) {
				int wvid = dicts.lookupIndex(WORDVEC, forms[i]);
				if (wvid <= 0) wvid = dicts.lookupIndex(WORDVEC, forms[i].toLowerCase());
				if (wvid > 0) wordVecIds[i] = wvid; else wordVecIds[i] = -1; 
			}
		}
		
		// set special pos
		for (int i = lastNormalWord; i < length; ++i) {
			if (coarseMap.containsKey(postags[i])) {
				String cpos = coarseMap.get(postags[i]);
				if ((cpos.equals("CONJ")
						|| PossibleLang.Japanese == lang) && conjWord.contains(forms[i])) {
					specialPos[i] = SpecialPos.C;
				}
				else if (cpos.equals("ADP"))
					specialPos[i] = SpecialPos.P;
				else if (cpos.equals("."))
					specialPos[i] = SpecialPos.PNX;
				else if (cpos.equals("VERB"))
					specialPos[i] = SpecialPos.V;
				else
					specialPos[i] = SpecialPos.OTHER;
			}
			else {
				//System.out.println("Can't find coarse map: " + postags[i]);
				//coarseMap.put(postags[i], "X");
				specialPos[i] = getSpecialPos(forms[i], postags[i]);
			}
		}
    }

    // Heuristic rules to "guess" POS type based on the POS tag string 
    // This is an extended version of the rules in EGSTRA code
    // 	(http://groups.csail.mit.edu/nlp/egstra/).
    //
    private SpecialPos getSpecialPos(String form, String tag) {
    	if (tag.charAt(0) == 'v' || tag.charAt(0) == 'V')
    		return SpecialPos.V;
    	else if (tag.charAt(0) == 'n' || tag.charAt(0) == 'N')
    		return SpecialPos.N;
    	else if (tag.equalsIgnoreCase("cc") ||
    		tag.equalsIgnoreCase("conj") ||
    		tag.equalsIgnoreCase("kon") ||
    		tag.equalsIgnoreCase("conjunction"))
    		return SpecialPos.C;
    	else if (tag.equalsIgnoreCase("prep") ||
    			 tag.equalsIgnoreCase("preposition") ||
    			 tag.equals("IN"))
    		return SpecialPos.P;
    	else if (tag.equalsIgnoreCase("punc") ||
    			 tag.equals("$,") ||
    			 tag.equals("$.") ||
    			 tag.equals(",") ||
    			 tag.equals(";") ||
    			 Evaluator.puncRegex.matcher(form).matches())
    		return SpecialPos.PNX;
    	else
    		return SpecialPos.OTHER;
    }
	
    private String normalize(String s) {
		if(s!=null && s.matches("[0-9]+|[0-9]+\\.[0-9]+|[0-9]+[0-9,]+"))
		    return "<num>";
		return s;
    }

	@Override
	public String toString() {
		StringBuilder sb = new StringBuilder();
		for (int i = 1; i < length; ++i) {
			sb.append(i + "\t");
			sb.append(forms[i] + "\t");
			sb.append((lemmas != null && lemmas[i] != "" ? lemmas[i] : "_") + "\t");
			sb.append(cpostags[i] + "\t");
			sb.append(postags[i] + "\t");
			sb.append("_\t");
			sb.append(heads[i] + "\t");
			sb.append(deprels[i] + "\t_\t_");
			sb.append("\n");
		}
		sb.append("\n");
		return sb.toString();
	}
}

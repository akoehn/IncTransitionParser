// import org.scalacheck.Gen
// import org.scalacheck.Gen.{someOf, oneOf, const, resultOf, nonEmptyListOf,
//   pick, identifier, frequency}
// import org.scalacheck.Arbitrary.arbitrary
// import org.scalacheck.commands.Commands
// import org.scalacheck._


// import parser.decoding._
// import parser.{AbstractTransitionParser}

// import Prop.forAll

// import scala.collection.JavaConversions._

// import org.scalatest._
/*
class TransitionTest extends FlatSpec with Matchers {

  // First, test with compatible VNs
  val words = List("foo", "bar", "baz", AbstractTransitionParser.VN_NAME, AbstractTransitionParser.VN_NAME)
  val ts = new TransitionState(words, words, words)
  val tsint = ts.shift.shift.reduce(TransitionState.ReduceType.LEFT, null).shift.shift

  val ts1 = tsint.reduce(TransitionState.ReduceType.RIGHT, null).reduce(TransitionState.ReduceType.RIGHT).shift().reduce(TransitionState.ReduceType.RIGHT).reduce(TransitionState.ReduceType.ROOT)


  "A transition" should "always result in trees" in {
    for (slength <- 1 to 3) { // adjust the second number to your insecurity
      val words = List.fill(slength)("word")
      val pos : List[String] = List.fill(words.length)("foo")
      val ts = new TransitionState(words, pos, words)
      val finalStates = ShiftBasedTransitionParser.produce_final_states(Stream(ts), new util.HashSet[TIntArrayList]())
      var i: Int = 0
      finalStates.foreach((x) => {
        assert(ShiftBasedTransitionParser.isTree(x))
        i += 1
      })
      info("tested " + i.toString + " trees")
    }
  }

  "TAGLike extending" should "always result in trees" in {
    val pred = PredictHead(emptyState,"Ralston",null,"NNP","VVIRT",0)
    assertResult(Array(0,2,0), pred.heads) {pred.heads}
    val predpred = PredictHead(pred,"Purina",null,"NNP","VVIRT",1)
    assertResult(Array(0,3,4,0,1), predpred.heads) {predpred.heads}
    assert(ShiftBasedTransitionParser.isTree(predpred))
  }

  it should "work" in {
    val s1 = AttachNew(emptyState, "foo", "foo", "VBD", 0)
    assertResult(Array(0,0)){s1.heads}
    val s2 = PredictHead(s1, "V1", "V1", "V1", "VVIRT", 1)
    assertResult(Array(0,0,3,1)){s2.heads}
    val s3 = PredictHead(s2, "V2", "V2", "V2", "NVIRT", 1)
    assertResult(Array(0,0,4,5,1,1)){s3.heads}
    val s4 = ReplaceVN(s3, "V3", "V3", "V3", 5)
    assertResult(Array(0,0,5,4,1,1)){s4.heads}
    assertResult(Array("<root-POS>","VBD", "V1", "V2","V3","VVIRT")){s4.pos}
  }

  // "A Match" should "be perfect for same gold and test" in {
  //   for (slength <- 1 to 5) {
  //     val words = List.fill(slength)("word")
  //     val pos : List[String] = List.fill(words.length)("foo")
  //     val ts = new TransitionState(words, pos, words)
  //     val finalStates = TransitionParser.produce_final_states(List(ts))
  //     var i: Int = 0
  //     finalStates.foreach((x) => {
  //       val di = x.toInstanceUninstantiated()
  //       val bm = TransitionParser.bestMatch(di, di)
  //       // every element should be matched
  //       assert(bm.length == di.forms.length)
  //       val numErrors = TransitionParser.computeErrors(x, di)
  //       assert(numErrors == AttachmentErrors(0,0,0))
  //       i += 1
  //     })
  //     info("tested " + i.toString + " trees")
  //   }
  // }
/*
  "A match" should "be the optimal one" in {

    // 020232
    // ts1

    // 020223
    val ts2 = tsint.reduce(TransitionState.ReduceType.TWORIGHT).shift().reduce(TransitionState.ReduceType.RIGHT).reduce(TransitionState.ReduceType.RIGHT).reduce(TransitionState.ReduceType.ROOT)

    // 020224
    val ts3 = tsint.shift.reduce(TransitionState.ReduceType.RIGHT).reduce(TransitionState.ReduceType.TWORIGHT).reduce(TransitionState.ReduceType.RIGHT).reduce(TransitionState.ReduceType.ROOT)

    val bm1 = TransitionParser.bestMatch(ts1.toInstanceUninstantiated, ts2.toInstanceUninstantiated)
    assert(bm1.equals(List(0,1,2,3,5,4)), bm1)

    val bm2 = TransitionParser.bestMatch(ts1.toInstanceUninstantiated, ts3.toInstanceUninstantiated)
    assert(bm2.equals(List(0,1,2,3,5,4)), bm2)

    info("Switched VNs should be mapped correctly")
    val (goldInst, predInst) = TransitionParser.createTrainingInstances(ts1.toInstanceUninstantiated, ts2.toInstanceUninstantiated, bm1)

    assert(goldInst.heads.sameElements(predInst.heads), "Gold instance: " + goldInst.heads.mkString("/") + "  Pred Inst: " + predInst.heads.mkString("/"))

  }

  it should "not match incompatible VNs" in {
    val words = List("foo", "bar", "baz", TransitionState.VN_NAME, TransitionState.VN_NAME)
    val posTags = List("foo", "bar", "baz", "VN1", "VN2")
    val ts = new TransitionState(words, posTags, words)
    val tsint = ts.shift.shift.reduce(TransitionState.ReduceType.LEFT).shift.shift

        // 020232
    val ts1 = tsint.reduce(TransitionState.ReduceType.RIGHT).reduce(TransitionState.ReduceType.RIGHT).shift().reduce(TransitionState.ReduceType.RIGHT).reduce(TransitionState.ReduceType.ROOT)

    // 020223
    val ts2 = tsint.reduce(TransitionState.ReduceType.TWORIGHT).shift().reduce(TransitionState.ReduceType.RIGHT).reduce(TransitionState.ReduceType.RIGHT).reduce(TransitionState.ReduceType.ROOT)

    // 020224
    val ts3 = tsint.shift.reduce(TransitionState.ReduceType.RIGHT).reduce(TransitionState.ReduceType.TWORIGHT).reduce(TransitionState.ReduceType.RIGHT).reduce(TransitionState.ReduceType.ROOT)

    val bm1 = TransitionParser.bestMatch(ts1.toInstanceUninstantiated, ts2.toInstanceUninstantiated)
    assert(bm1.equals(List(0,1,2,3,4,5)), bm1)

    val (goldInst, predInst) = TransitionParser.createTrainingInstances(ts1.toInstanceUninstantiated, ts2.toInstanceUninstantiated, bm1)

    info("createTrainingInstance")

    val (predInst2, goldInst2) = TransitionParser.createTrainingInstances(ts2.toInstanceUninstantiated, ts1.toInstanceUninstantiated, List(0,1,2,3,5,-1))

    //                   heads    words

    // pred original: 02022 3    foo bar baz VN1 VN2
    //  mapping     : 01235-1
    // gold original: 02023 2    foo bar baz VN1 VN2

    // gold new     : 0 2 0 2  2 3 -1      foo bar baz  VN1  VN2 [INS]
    // pred new     : 0 2 0 2 -1 3  2      foo bar baz [INS] VN1 VN2

    // TODO Assertions
    //assert(goldInst2.heads.sameElements(predInst2.heads), "Gold instance: " + goldInst2.heads.mkString("/") + "  Pred Inst: " + predInst2.heads.mkString("/"))

/*

To test:
 - unmatched node (on both sides)
 - head adjustments
   - from normal to VN
   - from VN to later VN
 - circular Match a->b->c->a
  */
  }

  */

  it should "correctly work with unmatched VNs" in {
    info("one unmatchable VN in gold and pred")
    val ts_1 = new TransitionState(ts1)
    val ts_2 = new TransitionState(ts1)
    ts_1.words = List("root","a","b","c", TransitionState.VN_NAME+"1",TransitionState.VN_NAME+"2")
    ts_1.pos = List("root","a","b","c", TransitionState.VN_NAME+"1",TransitionState.VN_NAME+"2")
    ts_2.words = List("root","x","y","z", TransitionState.VN_NAME+"3",TransitionState.VN_NAME+"4")
    ts_2.pos = List("root","x","y","z", TransitionState.VN_NAME+"3",TransitionState.VN_NAME+"1")
    ts_1.heads = Array(0,5,0,4,5,2)
    ts_2.heads = Array(0,5,0,5,5,2)
    ts_1.deprel = Array("root","a","b","c", TransitionState.VN_NAME+"1",TransitionState.VN_NAME+"2")
    ts_2.deprel = Array("root","x","y","z", TransitionState.VN_NAME+"3",TransitionState.VN_NAME+"1")

    // first, ts_1 == predicted, ts_2 == gold
    val bm_unmatched = AbstractTransitionParser.bestMatch(ts_2.toInstanceUninstantiated,
                                                    ts_1.toInstanceUninstantiated)
    assert(bm_unmatched.equals(IntLists.immutable.of(0,1,2,3,5,-1)), "Mapping is wrong!")
    val (predInst_un, goldInst_un) = AbstractTransitionParser.createTrainingInstances(ts_1.toInstanceUninstantiated,
      ts_2.toInstanceUninstantiated,
      bm_unmatched)
    info("matching: " + bm_unmatched.toString)
    info("gold should be 0/5/0/5/5/2/-1, is " + goldInst_un.heads.mkString("/"))
    info("pred should be 0/6/0/5/-1/6/2, is " + predInst_un.heads.mkString("/"))
    assert(goldInst_un.heads.sameElements(Array(0,5,0,5,5,2,-1)), "gold didn't match")
    assert(predInst_un.heads.sameElements(Array(0,6,0,5,-1,6,2)), "pred didn't match")


    // switch two nodes
    val (predInst_sw, goldInst_sw) = AbstractTransitionParser.createTrainingInstances(ts_1.toInstanceUninstantiated,
      ts_2.toInstanceUninstantiated,
      IntLists.immutable.of(0,1,2,3,5,4))
    info("matching: 0,1,2,3,5,4")
    info("gold should be 0/4/0/4/2/4, is " + goldInst_sw.heads.mkString("/"))
    info("pred should be 0/5/0/4/5/2, is " + predInst_sw.heads.mkString("/"))
    assert(goldInst_sw.heads.sameElements(Array(0,4,0,4,2,4)), "gold didn't match")
    assert(predInst_sw.heads.sameElements(Array(0,5,0,4,5,2)), "pred didn't match")



  }
}

*/
/**


TODO

 - create 


  */











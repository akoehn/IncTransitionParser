import org.scalatest._
import parser.evaluation.Matching
import parser.AbstractTransitionParser.VN_UNMAPPED


class MatchingTest extends FlatSpec with Matchers {
  def tryMatching(gold: Seq[Int], pred: Seq[Int], lastWord: Int, intended_matching: Seq[Int]) = {
    val matching = Matching.bestMatch(gold, pred, lastWord)
    assert(matching.sameElements(intended_matching), s"incorrect matching: ${matching.mkString(", ")} ")
  }
  val goldHeads = List(-1,2,3,0)
  "A matching" should "match in sequence" in {
    tryMatching(goldHeads, List(-1,2,3,0), 1, List(0,1,2,3))
  }
  it should "match against sequence" in {
    tryMatching(goldHeads, List(-1,3,0,2), 1, List(0,1,3,2))
  }

  it should "not match incorrect stuff" in {
    tryMatching(goldHeads, List(-1,0,3,1), 1, List(0,1,VN_UNMAPPED, VN_UNMAPPED))
  }

}

name := "rbgparser"

version := "1.0"

resolvers += Resolver.mavenLocal

scalaVersion := "2.12.4"


libraryDependencies += "org.apache.commons" % "commons-lang3" % "3.8.1"

libraryDependencies += "net.sf.trove4j" % "trove4j" % "3.0.3"

libraryDependencies += "com.google.guava" % "guava" % "21.0"

libraryDependencies += "com.github.ben-manes.caffeine" % "caffeine" % "2.3.5"

libraryDependencies += "org.scalatest" % "scalatest_2.12" % "3.0.4" % "test"

libraryDependencies += "com.typesafe.scala-logging" %% "scala-logging" % "3.5.0"

libraryDependencies += "ch.qos.logback" % "logback-classic" % "1.1.2"

libraryDependencies += "org.eclipse.collections" % "eclipse-collections-api" % "7.1.0"

libraryDependencies += "org.eclipse.collections" % "eclipse-collections" % "7.1.0"

// libraryDependencies += "com.lihaoyi" %% "pprint" % "0.3.9"

libraryDependencies += "org.rogach" %% "scallop" % "3.1.0"

libraryDependencies += "com.google.code.findbugs" % "jsr305" % "3.0.0"

libraryDependencies += "org.scala-lang.modules" % "scala-java8-compat_2.12" % "0.8.0"

libraryDependencies += "com.lihaoyi" %% "scalatags" % "0.6.3"

libraryDependencies += "io.gitlab.nats" % "deptreeviz" % "0.3.0"

javacOptions ++= Seq("-source", "1.8", "-target", "1.8")

javaOptions += "-Xmx20G"

scalacOptions ++= Seq("-target:jvm-1.8", "-deprecation") //, "-opt:l:classpath" for scala 2.12

mainClass in assembly := Some("parser.main")

mainClass in Compile := Some("parser.main")

test in assembly := {}

fork in run := true

outputStrategy := Some(StdoutOutput)

assemblyMergeStrategy in assembly := {
  case PathList("org","w3c","dom", xs @ _*) => MergeStrategy.first
  case x =>
    val oldStrategy = (assemblyMergeStrategy in assembly).value
    oldStrategy(x)
}

//libraryDependencies += "org.scalacheck" %% "scalacheck" % "1.12.1" % "test"

//testOptions in Test += Tests.Argument(TestFrameworks.ScalaCheck, "-maxSize", "5", "-minSuccessfulTests", "33", "-workers", "1", "-verbosity", "1")

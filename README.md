# PreTra

This is a predictive incremental dependency parser, see [my PhD
thesis](https://arne.chark.eu/phd/) for in-depth discussion about the
way it works.

The code is derived from [RBGParser](https://github.com/taolei87/RBGParser/),
but only the scoring part is actually used.

To compile it, run `sbt assembly`.  You can then run
`java -jar target/scala-2.12/rbgparser-assembly-1.0.jar`
to run the parser.  It will alway print its current configuration
to stderr.  Use `--help` to get a (long) list of options.

## Scoring

PreTra has two scoring modes: The RBGParser based scorer (which in
turn is pretty similar to other scorers such as TurboParser and Mate)
and an LSTM-based first-order scorer (similar to Kiperwasser & Goldberg).

The first-order scorer uses Dynet and did not show increased accuracy
in my experiments. It has its own set of subcommands (`--lstm-opts`)
The first order scorer arguments should come after all other parser arguments.
To disable the first order scorer, use `--lstm-opts --nouse-first-order-scorer`.

## train, test, eval

Train a parser with `--train`.  The parser can be trained on complete
sentence and will match its own incremental output to the complete
sentence annotation for updates (see my thesis for details).
The trained model will be written to `--out-model-file`.  You can
actually also provide a `--model-file`, in that case the parser will
start with that instead of the default initialization (this is untested).

You can use incremental gold standards for training if you have them
-- but this kind of defeats the purpose of this parser.

Supply a conll file with the `train-file [train.conll]` argument.  The
other arguments have mostly sane defaults, but using `--augment-loss`
will give you an accuracy boost.

You can test the parser with `--test` and suppling a `--test-file`.
This runs the parser, and writes the output to `--out-file`.  By
default, it will also run the evaluation while parsing.  If you want
to measure the speed of the parser, use `--test-skip-evaluation` to
only parse without evaluation.

If you already have data, you can run `--evaluate` with an
`--eval-system-file` and an `--eval-gold-file`.

The results for all evaluation will be written to `--eval-output-file`.

If something does not work the way you think it should, you can enable
html traces with `--html-trace`.  Warning: this is *super slow* as it
renders an SVG for every sentence prefix.  I'm still fairly proud of
this capability.  Because I wrote this mainly for my internal debugging needs,
the tracing is currently hard-coded to write to `/tmp/transitionlogs/`.

With `--gold-always-in-test` you make sure that the structure with the
least amount of errors stays in the beam.  It is a bit of a misnomer
(for historical reasons) because it is not neccessarily the *gold*
tree that stays in the beam as the parser might fail to create the
gold structure.  This flag is used to obtain an upper bound for the parser
assuming search errors would not be a problem.

For more information, have a look at the help.

## results visualization

`scripts/plot_results.r` generates the type of figures I used in my
thesis from the evaluation output.  It has documentation comments.

For Questions, send me an e-mail :-)

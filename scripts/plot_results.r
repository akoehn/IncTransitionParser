#! /usr/bin/env r

## Author: Arne Köhn <arne@chark.eu>
## License: Apache 2.0

## Usage:
## ./plot_results.r [foo.evaluation]
## ./plot_results.r [foo.evaluation.stability]
## this script auto-detects whether you plot accuracy or stability evaluation.
## and adapts the output.
## It generates four files:
## - a tikz file for publication
## - a png for fast viewing
## both with and without a legend

## needs littler
## install.packages("ggplot2")
## install.packages("reshape2")

library(dplyr)

library(ggplot2)
library(reshape2)

library(tikzDevice)

inputFile <- argv[1]

## inputFile <- "testdata.data"

lvl <- c("incorr", "incorrpred", "corrpred", "corrip")

writetikz <- function(inputFile, labeled=FALSE, withLegend=TRUE) {
    ratio <- 7*0.8
    data <- read.table(inputFile,header=TRUE,stringsAsFactors=FALSE)
    if (labeled) {
        data <- data[c("frontier", gsub("$", "l", lvl))]
        data <- data %>% rename_all(~gsub("l$", "", .))
    } else {
        data <- data[c("frontier", lvl)]
    }
    ## brks <- c(0, 0.25, 0.5, 0.75, 1)
    brks <- c(0.2, 0.4, 0.6, 0.8, 1)
    ylabels  <- gsub("\\.0", "", gsub("%", "\\%", scales::percent(brks),fixed=TRUE))

    ## cpalette <- c("incorr" = "#CC0000", "incorrpred" = "#CC8888", "corrpred" = "#22CC66", "corrip" = "#00EE00")
    cpalette <- c("incorr" = "#801815", "incorrpred" = "#D46D6A", "corrpred" = "#54A759", "corrip" = "#116416")
    ## http://colorbrewer2.org/?type=diverging&scheme=PuOr&n=4
    cpalette <- c("incorr" = "#e66101", "incorrpred" = "#fdb863", "corrpred" = "#b2abd2", "corrip" = "#5e3c99")

    clabels <- c("incorr" = "wrong", "incorrpred" = "wrong p", "corrpred" = "corr p", "corrip" = "corr")
    melted  <- melt(data,id.vars="frontier", value.name = "percent")
    melted$variable <- factor(melted$variable, levels = lvl)

    if (grepl(".stability", inputFile)) {
        # do not show complete stability as it is always 100%
        melted <- melted %>% filter(frontier < 6)
        yaxisname <- "\\footnotesize{stability}"
        ratio <- 6*0.8
    } else {
        yaxisname <- "\\footnotesize{accuracy}"
    }
    
    res <- ggplot(melted) +
        geom_bar(aes(x=frontier, y=percent, fill=variable), stat="identity", position="fill") +
        scale_x_continuous(breaks=seq(0,6,1), labels=c(seq(0,5,1), "comp."), name="\\footnotesize{dist. from newest word}") +
        scale_fill_manual(values=cpalette, labels=clabels) +
        scale_y_continuous(breaks = brks, labels = ylabels, name=yaxisname) +
        coord_fixed(ratio=ratio, ylim=c(0.2,1.0))

    
    if (!withLegend)
        res <- res + theme(legend.position="none")
    else {
        res <- res + theme(legend.position = "bottom") + guides(fill=guide_legend(nrow=2,byrow=TRUE, title=""))
    }
    outputFile <- paste0(inputFile,
                         if (labeled) "-labeled" else "",
                         if (withLegend) "" else "-nolegend",
                         ".tikz")
    ggsave(paste0(inputFile,
                         if (labeled) "-labeled" else "",
                         if (withLegend) "" else "-nolegend",
                         ".png"))
    tikz(file = outputFile, standAlone=F, width=2.3, height=1.7, pointsize=11)
    print(res)
    dev.off()
}

## for (labeled in c(FALSE, TRUE)){
##     for (withLegend in c(TRUE, FALSE)){
##          writetikz(inputFile, labeled, FALSE)
##     }
## }
writetikz(inputFile, FALSE, FALSE)
